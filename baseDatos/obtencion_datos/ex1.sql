/* Obtener los nombres de los productos de la tienda*/
SELECT nombre FROM  articulos;

/* Obtener los nombres y los precios de los productos de la tienda*/
SELECT nombre, precio FROM  articulos;

/* Obtener los nombres de los productos cuyo precio sea menor o igual a 200€*/
SELECT nombre,precio FROM articulos WHERE precio <= 200;

/* Obtener los nombres de los productos cuyo precio sea este entre los 60€ y los 120€*/
SELECT nombre, precio FROM articulos WHERE precio >= 60 AND precio <= 120;

/* Obtener los nombres y el precio en pesetas*/
SELECT nombre, precio*166.386 FROM articulos;

/* Obtener el precio medio de los productos*/
SELECT AVG(precio) FROM articulos;

/* Obtener el precio medio de los productos cuyo codigo de fabricante sea 2*/
SELECT AVG(a.precio) FROM articulos a, fabricantes f WHERE f.codigo = 2;

/* Obtener el numero de articulos cuyo precio sea mayor o igual a 180€*/
SELECT COUNT(codigo) FROM articulos WHERE precio >= 180;

/*1.9*/
SELECT nombre, precio FROM articulos WHERE precio >= 180 ORDER BY precio DESC, nombre ASC;

/*1.10*/
SELECT a.*, f.* FROM articulos a, fabricantes f WHERE f.codigo = 2 AND a.fabricante = 2;

/*1.11*/
SELECT a.nombre, a.precio, f.nombre FROM articulos a, fabricantes f WHERE f.codigo = a.fabricante;

/*1.12*/
SELECT AVG(a.precio), f.codigo FROM articulos a, fabricantes f WHERE f.codigo = a.fabricante GROUP BY f.codigo;

/*1.13*/
SELECT AVG(a.precio), f.nombre FROM articulos a, fabricantes f WHERE f.codigo = a.fabricante GROUP BY f.codigo;

/*1.14*/
SELECT f.nombre, AVG(a.precio) FROM articulos a, fabricantes f WHERE a.fabricante = f.codigo GROUP BY f.codigo HAVING AVG(a.precio) >= 150;

/*1.15*/
SELECT nombre, precio FROM articulos WHERE precio = (SELECT min(precio) FROM articulos);

/*1.16*/
select a.nombre, a.precio, f.nombre FROM articulos a, fabricantes f WHERE a.fabricante = f.codigo AND a.precio = (select max(a.precio) from articulos a where a.fabricante = f.codigo);

/*1.17 ALTAVOCES DE 70€ FABRICANTE 2*/
INSERT INTO articulos VALUES (11,'Altavoces', 70.00, 2);

/*1.18 ALTAVOCES DE 70€ FABRICANTE 2*/
UPDATE articulos SET nombre = 'Impresora Laser' WHERE codigo = 8;

/*1.19 Aplicar un descuento de 10% */
UPDATE articulos SET precio = precio * 0.9;
