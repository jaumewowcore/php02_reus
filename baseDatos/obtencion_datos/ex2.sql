/*2.1*/
SELECT apellidos FROM empleados;

/*2.2*/
SELECT apellidos FROM empleados GROUP BY apellidos;

/*2.3*/
SELECT apellidos FROM empleados GROUP BY apellidos;

/*2.4*/
SELECT * FROM empleados WHERE apellidos in('López', 'Pérez');

/*2.5*/
SELECT * FROM empleados WHERE departamento in(14);

/*2.6*/
SELECT * FROM empleados WHERE departamento in(14, 77);

/*2.7*/
SELECT * FROM empleados WHERE apellidos LIKE 'p%';

/*2.8*/
SELECT SUM(presupuesto) FROM departamentos;

/*2.9*/
SELECT d.nombre, COUNT(e.dni) FROM departamentos d, empleados e WHERE e.departamento = d.codigo GROUP BY d.codigo;

/*2.10*/
SELECT e.*, d.* FROM departamentos d, empleados e WHERE e.departamento = d.codigo;

/*2.11*/
SELECT e.nombre, e.apellidos, d.nombre, d.presupuesto FROM departamentos d, empleados e WHERE e.departamento = d.codigo;

/*2.12*/
SELECT e.nombre, e.apellidos FROM empleados e, departamentos d WHERE d.presupuesto > 60000 GROUP BY e.dni;

/*2.13*/
select * FROM departamentos WHERE presupuesto > (select AVG(presupuesto) FROM departamentos);

/*2.14*/
SELECT d.nombre FROM departamentos d, empleados e WHERE e.departamento = d.codigo GROUP BY codigo HAVING COUNT(e.dni) > 2;

/*2.15*/
INSERT INTO departamentos VALUES (11,'Calidad', 40000);
INSERT INTO empleados VALUES (89267109,'Esther', 'Vázquez', 11);

/*2.16*/
UPDATE departamentos SET presupuesto = presupuesto * 0.9;

/*2.17*/
UPDATE empleados SET departamento = 77 WHERE departamento = 14;

/*2.18*/
DELETE FROM empleados WHERE departamento = 14;

/*2.19*/
DELETE FROM empleados
WHERE EXISTS
    (SELECT *
        FROM departamentos
        WHERE empleados.departamento = departamentos.codigo 
        AND departamentos.presupuesto > 60000
);

/*2.20*/
DELETE FROM empleados;


