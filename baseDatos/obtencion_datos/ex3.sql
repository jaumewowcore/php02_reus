/*3.1*/
SELECT * FROM almacenes;

/*3.2*/
SELECT * FROM cajas WHERE valor > 150;

/*3.3*/
SELECT contenido FROM cajas GROUP BY contenido;

/*3.4*/
SELECT AVG(valor) AS "Valor_medio" FROM cajas;

/*3.5*/
SELECT AVG(c.valor) AS "Valor_medio", a.lugar FROM cajas c, almacenes a WHERE c.almacen = a.codigo GROUP BY a.codigo ;

/*3.6*/
SELECT a.codigo FROM almacenes a, cajas c WHERE a.codigo = c.almacen GROUP BY a.codigo HAVING AVG(c.valor) > 150;

/*3.7*/
SELECT c.numreferencia, a.lugar FROM almacenes a, cajas c WHERE a.codigo = c.almacen;

/*3.8*/
SELECT count(c.numreferencia) FROM almacenes a, cajas c WHERE a.codigo = c.almacen GROUP BY a.codigo;

/*3.9*/
SELECT codigo FROM almacenes WHERE capacidad < (select COUNT(numreferencia) from cajas WHERE codigo = almacen);

/*3.10*/
SELECT c.numreferencia FROM almacenes a, cajas c WHERE a.codigo = c.almacen AND a.lugar in('Bilbao');

/*3.11*/
INSERT INTO almacenes VALUES (6,'Barcelona', 3);

/*3.12*/
INSERT INTO cajas VALUES ('H5RT','Papel', 200, 2);

/*3.13*/
UPDATE cajas SET valor = valor * 0.85;

/*3.14*/
UPDATE cajas SET valor = valor*0.8 WHERE valor >
		(SELECT AVG(cajas.valor));

/*3.15*/
DELETE FROM cajas WHERE valor < 100;


/*3.16*/
DELETE cajas FROM almacenes
LEFT JOIN cajas ON almacenes.codigo = cajas.almacen WHERE 
(SELECT COUNT(cajas.numreferencia)) > almacenes.capacidad;