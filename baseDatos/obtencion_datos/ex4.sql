/*Salas*/
INSERT INTO salas VALUES (7, 'Gavarres1', '21');
INSERT INTO salas VALUES (8, 'Gavarres2', '21');
INSERT INTO salas VALUES (9, 'Gavarres3', '20');
INSERT INTO salas VALUES (10, 'Gavarres3', '25');
INSERT INTO salas VALUES (11, 'Tarragona', '22');
INSERT INTO salas VALUES (12, 'Valls', '21');
INSERT INTO salas VALUES (13, 'Montblanc', '21');
INSERT INTO salas VALUES (14, 'Lleida', '26');
INSERT INTO salas VALUES (15, 'Girona', '26');
INSERT INTO salas VALUES (16, 'Vilafranca', '28');


/*Peliculas*/
INSERT INTO peliculas VALUES (20,'cuatro', 7);
INSERT INTO peliculas VALUES (21,'CINCO', 7);
INSERT INTO peliculas VALUES (22,'seis', 'G');
INSERT INTO peliculas VALUES (23,'siete', 7);
INSERT INTO peliculas VALUES (24,'ocho', 7);
INSERT INTO peliculas VALUES (25,'nueve', 18);
INSERT INTO peliculas VALUES (26,'die', 16);
INSERT INTO peliculas VALUES (27,'onze', 18);
INSERT INTO peliculas VALUES (28,'doze', 16);
INSERT INTO peliculas VALUES (29,'treze', 16);



/*4.1*/
SELECT nombre FROM peliculas GROUP BY nombre;

/*4.2*/
SELECT DISTINCT calificacionEdad FROM peliculas;

/*4.3*/
SELECT nombre FROM peliculas WHERE calificacionEdad IS NULL;

/*4.4*/
SELECT nombre FROM salas WHERE pelicula IS NULL;

/*4.5*/
SELECT * FROM salas LEFT JOIN peliculas ON salas.pelicula = peliculas.codigo;

/*4.6*/
SELECT * FROM salas RIGHT JOIN peliculas ON salas.pelicula = peliculas.codigo;

/*4.7*/
SELECT peliculas.nombre FROM salas RIGHT JOIN peliculas ON salas.pelicula = peliculas.codigo
WHERE salas.pelicula IS NULL;

/*4.8*/
INSERT INTO peliculas VALUES (10,'Uno', 7);
INSERT INTO peliculas VALUES (11,'Dos', 7);
INSERT INTO peliculas VALUES (12,'Tres', 7);

/*4.9*/
UPDATE peliculas SET calificacionEdad=13 WHERE calificacionEdad IS NULL;

/*4.10*/
DELETE FROM salas WHERE pelicula IN (SELECT codigo FROM peliculas WHERE peliculas.calificacionEdad IN('G'));
