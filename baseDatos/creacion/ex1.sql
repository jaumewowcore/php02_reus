/* CREAR LA BASE DE DATOS */
CREATE DATABASE meteo;
USE meteo;

/* Estacion */
CREATE TABLE IF NOT EXISTS estacion (
    identificador MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
    latitud VARCHAR(14) NOT NULL,
    longitud VARCHAR(15) NOT NULL,
    altitud MEDIUMINT NOT NULL,
    PRIMARY KEY (identificador)
)ENGINE=InnoDB;

/* muestra */
CREATE TABLE IF NOT EXISTS muestra (
    identificadorestacion MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT,
    fecha DATE NOT NULL,
    temperaturaminima TINYINT,
    temperaturamaxima TINYINT,
    precipitaciones SMALLINT UNSIGNED,
    humedadminima TINYINT UNSIGNED,
    humedadmaxima TINYINT UNSIGNED,
    velocidadminima SMALLINT UNSIGNED,
    velocidadmaxima SMALLINT UNSIGNED,
    KEY (identificadorestacion),
    FOREIGN KEY (identificadorestacion) REFERENCES estacion (identificador)
    ON DELETE NO ACTION ON UPDATE CASCADE
)ENGINE=InnoDB;