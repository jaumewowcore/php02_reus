/* CREAR LA BASE DE DATOS */
CREATE DATABASE almacenes;
USE almacenes;

/* tabla almacen*/
CREATE TABLE IF NOT EXISTS almacen (
    id MEDIUMINT NOT NULL AUTO_INCREMENT,
    lugar VARCHAR(100) NOT NULL,    
    capacidad MEDIUMINT UNSIGNED,      
    PRIMARY KEY(id) 
)ENGINE=InnoDB;

/* tabla caja*/
CREATE TABLE IF NOT EXISTS caja (
    numReferencia VARCHAR(5) NOT NULL,
    contenido VARCHAR(100) NOT NULL,
    valor SMALLINT,
    idAlmacen MEDIUMINT,
    PRIMARY KEY (numReferencia),
    FOREIGN KEY (idAlmacen) REFERENCES almacen(id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)ENGINE=InnoDB;