/* CREAR LA BASE DE DATOS */
CREATE DATABASE piezas_proveedores;
USE piezas_proveedores;

/* tabla pieza*/
CREATE TABLE IF NOT EXISTS pieza (
    id TINYINT UNSIGNED AUTO_INCREMENT,
    nombre VARCHAR(100) NOT NULL,    
    PRIMARY KEY(id) 
)ENGINE=InnoDB;

/* tabla proveedor*/
CREATE TABLE IF NOT EXISTS proveedor (
    id char(4),
    nombre VARCHAR(100) NOT NULL,    
    PRIMARY KEY (id)
)ENGINE=InnoDB;

/* tabla suministra*/
CREATE TABLE IF NOT EXISTS suministra (
    idPieza TINYINT UNSIGNED,
    idProveedor char(4) NOT NULL,
    precio  INT NOT NULL,    
    FOREIGN KEY (idPieza) REFERENCES pieza(id)
     ON DELETE CASCADE
    ON UPDATE CASCADE,
    FOREIGN KEY (idProveedor) REFERENCES proveedor(id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)ENGINE=InnoDB;