/* CREAR LA BASE DE DATOS */
CREATE DATABASE direccion;
USE direccion;

/* tabla despacho*/
CREATE TABLE IF NOT EXISTS despacho (
    id TINYINT UNSIGNED AUTO_INCREMENT,
    capacidad MEDIUMINT NOT NULL,    
    PRIMARY KEY(id) 
)ENGINE=InnoDB;

/* tabla director*/
CREATE TABLE IF NOT EXISTS director (
    dni VARCHAR(8),
    PRIMARY KEY (dni),
    dniJefe VARCHAR(8),
    idDespacho TINYINT UNSIGNED,
    FOREIGN KEY (dniJefe) REFERENCES director(dni)
    ON DELETE CASCADE
    ON UPDATE CASCADE,    
    FOREIGN KEY (idDespacho) REFERENCES despacho(id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)ENGINE=InnoDB;