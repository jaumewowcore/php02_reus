/* CREAR LA BASE DE DATOS */
CREATE DATABASE escuela;
USE escuela;

/* tabla profesor*/
CREATE TABLE IF NOT EXISTS profesor (
    dni VARCHAR(8),    
    nombre VARCHAR(100) UNIQUE NOT NULL,    
    apellido1 VARCHAR(100),    
    apellido2 VARCHAR(100),    
    direccion VARCHAR(255) NOT NULL,    
    titulo VARCHAR(255) NOT NULL,    
    gana SMALLINT NOT NULL,
    PRIMARY KEY(dni) 
)ENGINE=InnoDB;

/* tabla curso*/
CREATE TABLE IF NOT EXISTS curso (
    cod_curso INT,    
    nombre_curso VARCHAR(100) UNIQUE NOT NULL,  
    maximo_alumnos SMALLINT NOT NULL,  
    horas TIME NOT NULL,    
    fecha_inicio DATETIME NOT NULL,
    fecha_fin DATETIME NOT NULL,
    CHECK(fecha_fin >= fecha_inicio),
    dni_profesor VARCHAR(8) NOT NULL,  
    PRIMARY KEY(cod_curso),
    FOREIGN KEY (dni_profesor) REFERENCES profesor(dni)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)ENGINE=InnoDB;

/* tabla alumno*/
CREATE TABLE IF NOT EXISTS alumno (
    dni VARCHAR(8),    
    nombre VARCHAR(100) UNIQUE NOT NULL,    
    apellido1 VARCHAR(100),    
    apellido2 VARCHAR(100),    
    direccion VARCHAR(255) NOT NULL,    
    fecha_nacimiento DATE,
    sexo CHAR(1) NOT NULL CHECK(sexo in ('M', 'F')),
    curso INT NOT NULL,
    PRIMARY KEY(dni),
    FOREIGN KEY (curso) REFERENCES curso(cod_curso)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)ENGINE=InnoDB;