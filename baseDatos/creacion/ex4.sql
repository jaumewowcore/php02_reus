/* CREAR LA BASE DE DATOS */
CREATE DATABASE empleados;
USE empleados;

/* tabla departamento*/
CREATE TABLE IF NOT EXISTS departamento (
    id MEDIUMINT NOT NULL AUTO_INCREMENT,
    nombre VARCHAR(100) NOT NULL,    
    presupuesto MEDIUMINT UNSIGNED,      
    PRIMARY KEY(id) 
)ENGINE=InnoDB;

/* tabla empleado*/
CREATE TABLE IF NOT EXISTS empleado (
    dni VARCHAR(8) NOT NULL,
    nombre VARCHAR(100) NOT NULL,
    apellidos VARCHAR(100),
    idDepartamento MEDIUMINT,
    PRIMARY KEY (dni),
    FOREIGN KEY (idDepartamento) REFERENCES departamento(id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)ENGINE=InnoDB;