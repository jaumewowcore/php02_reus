/* CREAR LA BASE DE DATOS */
CREATE DATABASE cientificos;
USE cientificos;

/* tabla cientifico*/
CREATE TABLE IF NOT EXISTS cientifico (
    dni VARCHAR(8),
    nomApels VARCHAR(255) NOT NULL,    
    PRIMARY KEY(dni) 
)ENGINE=InnoDB;

/* tabla proyecto*/
CREATE TABLE IF NOT EXISTS proyecto (
    id CHAR(4),
    nombre VARCHAR(255) NOT NULL,    
    horas INT NOT NULL,    
    PRIMARY KEY(id) 
)ENGINE=InnoDB;

/* tabla assignado_a*/
CREATE TABLE IF NOT EXISTS assignado_a (
    cientifico VARCHAR(8),
    proyecto CHAR(4),
    FOREIGN KEY (cientifico) REFERENCES cientifico(dni)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
    FOREIGN KEY (proyecto) REFERENCES proyecto(id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)ENGINE=InnoDB;

