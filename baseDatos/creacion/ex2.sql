/* CREAR LA BASE DE DATOS */
CREATE DATABASE biblio;
USE biblio;

/* editorial */
CREATE TABLE IF NOT EXISTS editorial (
    claveeditorial SMALLINT NOT NULL AUTO_INCREMENT,
    nombre VARCHAR(60) NOT NULL,
    direccion VARCHAR(60) NOT NULL,
    telefono VARCHAR(15) NOT NULL,
    PRIMARY KEY (claveeditorial)
)ENGINE=InnoDB;

/* Libro */
CREATE TABLE IF NOT EXISTS libro (
    clavelibro INT NOT NULL AUTO_INCREMENT,
    titulo VARCHAR(60) NOT NULL,
    idioma VARCHAR(60) NOT NULL,
    formato VARCHAR(15) NOT NULL,
    claveeditorial SMALLINT,
    PRIMARY KEY (clavelibro),
    FOREIGN KEY (claveeditorial) REFERENCES editorial(claveeditorial)
    ON DELETE SET NULL
    ON UPDATE CASCADE
)ENGINE=InnoDB;

/* Tema */
CREATE TABLE IF NOT EXISTS tema (
    clavetema SMALLINT NOT NULL AUTO_INCREMENT,
    nombre VARCHAR(40) NOT NULL,
    PRIMARY KEY (clavetema)
)ENGINE=InnoDB;

/* Autor */
CREATE TABLE IF NOT EXISTS autor (
    claveautor INT NOT NULL AUTO_INCREMENT,
    nombre VARCHAR(60) NOT NULL,
    PRIMARY KEY (claveautor)
)ENGINE=InnoDB;

/* Ejemplar */
CREATE TABLE IF NOT EXISTS ejemplar (
    claveejemplar INT NOT NULL AUTO_INCREMENT,
    clavelibro INT NOT NULL,
    numeroorden SMALLINT NOT NULL,
    edicion SMALLINT,
    ubicacion VARCHAR(15),
    categoria CHAR,
    PRIMARY KEY (claveejemplar),
    FOREIGN KEY (clavelibro) REFERENCES libro(clavelibro)
    ON DELETE CASCADE ON UPDATE CASCADE
)ENGINE=InnoDB;

/* Socio */
CREATE TABLE IF NOT EXISTS socio (
    clavesocio INT NOT NULL AUTO_INCREMENT,
    nombre VARCHAR(60),
    telefono VARCHAR(15),
    categoria CHAR,
    PRIMARY KEY (clavesocio)
)ENGINE=InnoDB;

/* Prestamo */
CREATE TABLE IF NOT EXISTS prestamo (
    clavesocio INT,
    claveejemplar INT,
    numeroorden SMALLINT,
    fecha_prestamo DATE NOT NULL,
    fecha_devolucion DATE NOT NULL,
    notas BLOB,
    FOREIGN KEY (clavesocio) REFERENCES socio(clavesocio)
    ON DELETE SET NULLON UPDATE CASCADE,
    FOREIGN KEY (claveejemplar) REFERENCES ejemplar(claveejemplar)
    ON DELETE SET NULLON UPDATE CASCADE
)ENGINE=InnoDB;

/* trata_sobre */
CREATE TABLE IF NOT EXISTS trata_sobre (
    clavelibro INT NOT NULL,
    clavetema SMALLINT NOT NULL,
    FOREIGN KEY (clavelibro) REFERENCES libro(clavelibro)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
    FOREIGN KEY (clavetema) REFERENCES tema(clavetema)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)ENGINE=InnoDB;

/* escrito_por */
CREATE TABLE IF NOT EXISTS escrito_por (
    clavelibro INT NOT NULL,
    claveautor INT NOT NULL,
    FOREIGN KEY (clavelibro) REFERENCES libro(clavelibro)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
    FOREIGN KEY (claveautor) REFERENCES autor(claveautor)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)ENGINE=InnoDB;