/* CREAR LA BASE DE DATOS */
CREATE DATABASE cinema;
USE cinema;

/* tabla pelicula*/
CREATE TABLE IF NOT EXISTS pelicula (
    id INT NOT NULL AUTO_INCREMENT,
    nombre VARCHAR(100) NOT NULL,    
    calificacionEdad TINYINT UNSIGNED,      
    PRIMARY KEY(id) 
)ENGINE=InnoDB;

/* tabla sala*/
CREATE TABLE IF NOT EXISTS sala (
    id INT NOT NULL AUTO_INCREMENT,
    nombre VARCHAR(100) NOT NULL,
    idPelicula INT,
    PRIMARY KEY (id),
    FOREIGN KEY (idPelicula) REFERENCES pelicula(id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)ENGINE=InnoDB;