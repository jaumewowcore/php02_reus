/* CREAR LA BASE DE DATOS */
CREATE DATABASE tienda_informatica;
USE tienda_informatica;

/* tabla fabricante*/
CREATE TABLE IF NOT EXISTS fabricante (
    id SMALLINT NOT NULL AUTO_INCREMENT,
    nombre VARCHAR(100) NOT NULL,    
    PRIMARY KEY (id)
)ENGINE=InnoDB;

/* tabla articulo*/
CREATE TABLE IF NOT EXISTS articulo (
    id MEDIUMINT NOT NULL AUTO_INCREMENT,
    nombre DATE NOT NULL,
    idfabricante SMALLINT,
    PRIMARY KEY (id),
    FOREIGN KEY (idfabricante) REFERENCES fabricante(id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)ENGINE=InnoDB;