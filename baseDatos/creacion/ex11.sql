/* CREAR LA BASE DE DATOS */
CREATE DATABASE investigadores;
USE investigadores;

/* tabla facultad*/
CREATE TABLE IF NOT EXISTS facultad (
    id INT,
    nombre VARCHAR(100) NOT NULL,    
    PRIMARY KEY(id) 
)ENGINE=InnoDB;


/* tabla investigador*/
CREATE TABLE IF NOT EXISTS investigador (
    dni VARCHAR(8),
    nomApels VARCHAR(255),
    facultad INT NOT NULL,
    PRIMARY KEY(dni),
    FOREIGN KEY (facultad) REFERENCES facultad(id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)ENGINE=InnoDB;

/* tabla equipo*/
CREATE TABLE IF NOT EXISTS equipo (
    numSerie CHAR(4),
    nombre VARCHAR(100) NOT NULL,
    facultad INT NOT NULL,
    PRIMARY KEY(numSerie),
    FOREIGN KEY (facultad) REFERENCES facultad(id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)ENGINE=InnoDB;

/* tabla reserva*/
CREATE TABLE IF NOT EXISTS reserva (
    dni VARCHAR(8) NOT NULL,
    numSerie CHAR(4) NOT NULL,
    comienzo DATETIME NOT NULL,
    fin DATETIME NOT NULL,
    CHECK(fin >= comienzo),
    FOREIGN KEY (dni) REFERENCES investigador(dni)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
    FOREIGN KEY (numSerie) REFERENCES equipo(numSerie)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)ENGINE=InnoDB;


