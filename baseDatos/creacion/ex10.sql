/* CREAR LA BASE DE DATOS */
CREATE DATABASE grandes_almacenes;
USE grandes_almacenes;

/* tabla cajero*/
CREATE TABLE IF NOT EXISTS cajero (
    id INT,
    nomApels VARCHAR(255) NOT NULL,    
    PRIMARY KEY(id) 
)ENGINE=InnoDB;

/* tabla producto*/
CREATE TABLE IF NOT EXISTS producto (
    id INT,
    nombre VARCHAR(255) NOT NULL,    
    precio MEDIUMINT NOT NULL,    
    PRIMARY KEY(id) 
)ENGINE=InnoDB;

/* tabla maquina_registradora*/
CREATE TABLE IF NOT EXISTS maquina_registradora (
    id INT,
    piso INT NOT NULL,
    PRIMARY KEY(id) 
)ENGINE=InnoDB;

/* tabla venta*/
CREATE TABLE IF NOT EXISTS venta (
    cajero INT,
    maquina INT,
    producto INT,
    FOREIGN KEY (cajero) REFERENCES cajero(id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
    FOREIGN KEY (maquina) REFERENCES maquina_registradora(id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
    FOREIGN KEY (producto) REFERENCES producto(id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)ENGINE=InnoDB;