var cadena;

//Coger los datos por el usuario
cadena = prompt("Escribe una palabra", "palabra");

//Mostrar el resultado llamando la funcion
alert(analizar(cadena));


//Funcion analizar recorre las letras y comprueba si es majus o minus
function analizar(cadena) {
    var message = "cadena erronea";

    for (i = 0; i < cadena.length; i++) {

        //Comprovación de Majuscula
        if (cadena[i] === cadena[i].toUpperCase()) {
            contMajus++;
        }

        //Comprovación de Minúscula
        if (cadena[i] === cadena[i].toLowerCase()) {
            contMinus++;
        }

    }


    if (contMajus >= 1) {
        message = "cadena con majúsculas";
    }

    if (contMinus >= 1) {
        message = "cadena con minúsculas";
    }

    if (contMajus >= 1 && contMinus >= 1) {
        message = "cadena con majúsculas y minúsculas";

    }

    return message;
}