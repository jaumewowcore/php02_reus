var dado1, dado2;
var lanzamientos = 36000;

var numeros = [0, 0, 0, 0, 0, 0];

//bucle para todos los lanzamientos
while (lanzamientos != 0) {
    //Generar numeros aleatorios
    dado1 = Math.floor(Math.random() * 6) + 1
    dado2 = Math.floor(Math.random() * 6) + 1

    //Assignar cada numero en su posicion
    for (let i = 0; i < 7; i++) {
        if (dado1 == i) {
            numeros[i - 1] = numeros[i - 1] + 1;
        }

        if (dado2 == i) {
            numeros[i - 1] = numeros[i - 1] + 1;
        }
    }

    lanzamientos--;
}

//Mostrar cuantos tiro de cada numero hay
for (let i = 0; i < numeros.length; i++) {
    alert("El numero " + (i + 1) + " ha salido " + numeros[i] + " veces.");
}