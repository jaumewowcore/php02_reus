var letras = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F',
    'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H',
    'L', 'C', 'K', 'E', 'T'
];

var dni = +prompt("Dime el numero del DNI");
var letra = prompt("Dime la letra");

if (dni < 0 || dni > 99999999) {
    alert("el numero proporcionado no es válido");

} else {
    dni = parseInt(dni % 23);
    dni = letras[dni];

    if (letra == dni) {
        alert("el numero i la letra son correctos");

    } else {
        alert("la letra proporcionada no es válida");

    }
}