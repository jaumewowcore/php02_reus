var imatges = document.querySelectorAll("#imagenes img");
var gran = document.querySelector("#zumm");
var text = document.querySelector("#subtitul");

console.log(imatges);

imatges.forEach(imatge => {
    imatge.addEventListener("click", function() {
        gran.src = imatge.src;
        text.innerHTML = imatge.alt;
    });
});