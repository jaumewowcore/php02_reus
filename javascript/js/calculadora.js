var resultado = 0;
var accion = '';


var mostrador = $("#mostrador");
mostrador.val('0');

var retornar = $("#retornar");
var ce = $("#ce");
var c = $("#c");
var siete = $("#siete");
var ocho = $("#ocho");
var ocho = $("#ocho");
var nueve = $("#nueve");
var dividir = $("#dividir");
var raiz = $("#raiz");
var cuatro = $("#cuatro");
var cinco = $("#cinco");
var seis = $("#seis");
var multiplicar = $("#multiplicar");
var porcien = $("#porcien");
var uno = $("#uno");
var dos = $("#dos");
var tres = $("#tres");
var menos = $("#menos");
var fraccion = $("#fraccion");
var zero = $("#zero");
var signo = $("#signo");
var punto = $("#punto");
var mas = $("#mas");
var igual = $("#igual");

//Numeros
zero.click(function() {
    pasarNumero('0');
});


uno.click(function() {
    pasarNumero('1');
});


dos.click(function() {
    pasarNumero('2');
});

tres.click(function() {
    pasarNumero('3');
});

cuatro.click(function() {
    pasarNumero('4');
});

cinco.click(function() {
    pasarNumero('5');
});

seis.click(function() {
    pasarNumero('6');
});

siete.click(function() {
    pasarNumero('7');
});

ocho.click(function() {
    pasarNumero('8');
});

nueve.click(function() {
    pasarNumero('9');
});


//Quitar ultimo numero
retornar.click(function() {
    if ((mostrador.val().length - 1) >= -1) {
        mostrador.html(mostrador.val().substring(0, mostrador.val().length - 1));
        mostrador.val(mostrador.val().substring(0, mostrador.val().length - 1));
    } else {
        pasarNumero('0');
    }
});

//CE
ce.click(function() {
    mostrador.val(0);
    mostrador.html('0');
    accion = null;
});

//C
c.click(function() {
    mostrador.val(0);
    mostrador.html('0');
    resultado = 0;
    accion = null;
});

//Punto
punto.click(function() {
    let num = +mostrador.val();
    if (typeof num === 'number') {

        if (num % 1 === 0) {
            pasarNumero('.');
        }
    }
});

//Raiz
raiz.click(function() {

     if (resultado != 0) {
		let res = Math.sqrt(resultado);
        mostrador.val(res);
        mostrador.html(res);
        resultado = res;

    } else if (mostrador.val() != 0) {
    		let res = Math.sqrt(mostrador.val());
            mostrador.val(res);
            mostrador.text(res);
            resultado = res;
    }
});

//Suma
mas.click(function() {
    if (resultado != 0) {
        resultado += parseFloat(mostrador.val());
        mostrador.val(0);
        mostrador.html('');
        accion = 'sumar';
    } else {

        if (mostrador.val() != 0) {
            resultado = parseFloat(mostrador.val());
            mostrador.val(0);
        	mostrador.html('');
            accion = 'sumar';
        }
    }

});

//Resta
menos.click(function() {
    if (resultado != 0) {
        resultado -= parseFloat(mostrador.val());
        mostrador.val(0);
        mostrador.html('');
        accion = 'restar';
    } else {

        if (mostrador.val() != 0) {
            resultado = parseFloat(mostrador.val());
            mostrador.val(0);
        	mostrador.html('');
            accion = 'restar';
        }
    }

});

//Multiplicar
multiplicar.click(function() {
 	if (resultado != 0) {
        resultado *= parseFloat(mostrador.val());
        mostrador.val(0);
        mostrador.html('');
        accion = 'multiplicar';
    } else {

        if (mostrador.val() != 0) {
            resultado = parseFloat(mostrador.val());
            mostrador.val(0);
        	mostrador.html('');
            accion = 'multiplicar';
        }
    }

});

//dividir
dividir.click(function() {
    if (resultado != 0) {
        resultado /= parseFloat(mostrador.val());
        mostrador.val(0);
        mostrador.html('');
        accion = 'dividir';
    } else {

        if (mostrador.val() != 0) {
            resultado = parseFloat(mostrador.val());
            mostrador.val(0);
        	mostrador.html('');
            accion = 'dividir';
        }
    }

});


//Por ciento
porcien.click(function() {
    if (resultado != 0) {
        resultado = (parseFloat(mostrador.val() * resultado) / 100);
        mostrador.val(0);
        mostrador.html('');
    } else {

        if (mostrador.val() != 0) {
            resultado = parseFloat(mostrador.val());
            mostrador.val('0');
       		mostrador.html('');
        }
    }

});


//fraccion
fraccion.click(function() {
    if (mostrador.val() != 0) {
        resultado = 1 / parseFloat(mostrador.val());
        mostrador.val(resultado);
        mostrador.html(resultado);
        resultado = 0;
    }

});


//Igual
igual.click(function() {
    if (resultado != 0) {
        switch (accion) {
            case 'sumar':
                resultado = resultado + parseFloat(mostrador.val());
                mostrador.val('0');
                mostrador.html(resultado);
                break;

            case 'restar':
                resultado = resultado - parseFloat(mostrador.val());
                mostrador.val('0');
                mostrador.html(resultado);
                break;

            case 'dividir':
                resultado = resultado / parseFloat(mostrador.val());
                mostrador.val('0');
                mostrador.html(resultado);
                break;

            case 'multiplicar':
                resultado = resultado * parseFloat(mostrador.val());
                mostrador.val('0');
                mostrador.html(resultado);
                break;

        }

        accion = null;
        resultado = 0;
    }

});

//Canviar signo
signo.click(function() {
    let num = +mostrador.val();
    if (typeof num === 'number') {
        if (num >= 0) {
            mostrador.val(Math.abs(mostrador.val()));
        } else {
            mostrador.val((mostrador.val() * -1));
        }
    }
});


//Assignar numero i mostrar por pantalla
function pasarNumero(numero) {
    if (mostrador.val() == '0') {
        mostrador.val(numero);

    } else {
        mostrador.val(mostrador.val()+numero);
    }

    mostrador.html(mostrador.val());

}