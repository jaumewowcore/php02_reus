var resultado = 0;
var firts = true;
var accion = '';


var mostrador = document.getElementById("mostrador");
mostrador.value = '0';

var retornar = document.getElementById("retornar");
var ce = document.getElementById("ce");
var c = document.getElementById("c");
var siete = document.getElementById("siete");
var ocho = document.getElementById("ocho");
var ocho = document.getElementById("ocho");
var nueve = document.getElementById("nueve");
var dividir = document.getElementById("dividir");
var raiz = document.getElementById("raiz");
var cuatro = document.getElementById("cuatro");
var cinco = document.getElementById("cinco");
var seis = document.getElementById("seis");
var multiplicar = document.getElementById("multiplicar");
var porcien = document.getElementById("porcien");
var uno = document.getElementById("uno");
var dos = document.getElementById("dos");
var tres = document.getElementById("tres");
var menos = document.getElementById("menos");
var fraccion = document.getElementById("fraccion");
var zero = document.getElementById("zero");
var signo = document.getElementById("signo");
var punto = document.getElementById("punto");
var mas = document.getElementById("mas");
var igual = document.getElementById("igual");

zero.addEventListener("click", function() {
    pasarNumero('0');
});


uno.addEventListener("click", function() {
    pasarNumero('1');
});


dos.addEventListener("click", function() {
    pasarNumero('2');
});

tres.addEventListener("click", function() {
    pasarNumero('3');
});

cuatro.addEventListener("click", function() {
    pasarNumero('4');
});

cinco.addEventListener("click", function() {
    pasarNumero('5');
});

seis.addEventListener("click", function() {
    pasarNumero('6');
});

siete.addEventListener("click", function() {
    pasarNumero('7');
});

ocho.addEventListener("click", function() {
    pasarNumero('8');
});

nueve.addEventListener("click", function() {
    pasarNumero('9');
});


retornar.addEventListener("click", function() {
    if ((mostrador.value.length - 1) >= 0) {
        mostrador.innerHTML = mostrador.value.substring(0, mostrador.value.length - 1);
        mostrador.value = mostrador.value.substring(0, mostrador.value.length - 1);
    } else {
        pasarNumero('0');
    }
});

ce.addEventListener("click", function() {
    mostrador.value = 0;
    mostrador.innerHTML = '0';
    accion = null;
});

c.addEventListener("click", function() {
    mostrador.value = 0;
    mostrador.innerHTML = '0';
    resultado = 0;
    accion = null;
});

punto.addEventListener("click", function() {
    let num = +mostrador.value;
    console.log(num);
    if (typeof num === 'number') {

        if (num % 1 === 0) {
            pasarNumero('.');
        }
    }
});

raiz.addEventListener("click", function() {
    if (resultado != 0) {
        mostrador.value = Math.sqrt(resultado);
        mostrador.innerHTML = Math.sqrt(resultado);
        resultado = Math.sqrt(resultado);
    } else {

        if (mostrador.value != 0) {
            mostrador.value = Math.sqrt(mostrador.value);
            mostrador.innerHTML = Math.sqrt(mostrador.value);
            resultado = Math.sqrt(mostrador.value);
        }
    }

});

mas.addEventListener("click", function() {
    if (resultado != 0) {
        resultado += parseFloat(mostrador.value);
        mostrador.value = '0';
        mostrador.innerHTML = '';
        accion = 'sumar';
    } else {

        if (mostrador.value != 0) {
            resultado = parseFloat(mostrador.value);
            mostrador.value = '0';
            mostrador.innerHTML = '';
            accion = 'sumar';
        }
    }

});

menos.addEventListener("click", function() {
    if (resultado != 0) {
        resultado -= parseFloat(mostrador.value);
        mostrador.value = '0';
        mostrador.innerHTML = '';
        accion = 'restar';
    } else {

        if (mostrador.value != 0) {
            resultado = parseFloat(mostrador.value);
            mostrador.value = '0';
            mostrador.innerHTML = '';
            accion = 'restar';
        }
    }

});

multiplicar.addEventListener("click", function() {
    if (resultado != 0) {
        resultado *= parseFloat(mostrador.value);
        mostrador.value = '0';
        mostrador.innerHTML = '';
        accion = 'multiplicar';
    } else {

        if (mostrador.value != 0) {
            resultado = parseFloat(mostrador.value);
            mostrador.value = '0';
            mostrador.innerHTML = '';
            accion = 'multiplicar';
        }
    }

});

dividir.addEventListener("click", function() {
    if (resultado != 0) {
        resultado /= parseFloat(mostrador.value);
        mostrador.value = '0';
        mostrador.innerHTML = '';
        accion = 'dividir';
    } else {

        if (mostrador.value != 0) {
            resultado = parseFloat(mostrador.value);
            mostrador.value = '0';
            mostrador.innerHTML = '';
            accion = 'dividir';
        }
    }

});

porcien.addEventListener("click", function() {
    if (resultado != 0) {
        resultado = (parseFloat(mostrador.value) * resultado) / 100;
        mostrador.value = '0';
        mostrador.innerHTML = '';
    } else {

        if (mostrador.value != 0) {
            resultado = parseFloat(mostrador.value);
            mostrador.value = '0';
            mostrador.innerHTML = '';
        }
    }

});

fraccion.addEventListener("click", function() {
    if (mostrador.value != 0) {
        resultado = 1 / parseFloat(mostrador.value);
        mostrador.value = resultado;
        mostrador.innerHTML = resultado;
        resultado = 0;
    }

});


igual.addEventListener("click", function() {
    console.log(resultado);
    if (resultado != 0) {
        switch (accion) {
            case 'sumar':
                resultado = resultado + parseFloat(mostrador.value);
                mostrador.value = '0';
                mostrador.innerHTML = resultado;
                break;

            case 'restar':
                resultado = resultado - parseFloat(mostrador.value);
                mostrador.value = '0';
                mostrador.innerHTML = resultado;
                break;

            case 'dividir':
                resultado = resultado / parseFloat(mostrador.value);
                mostrador.value = '0';
                mostrador.innerHTML = resultado;
                break;

            case 'multiplicar':
                resultado = resultado * parseFloat(mostrador.value);
                mostrador.value = '0';
                mostrador.innerHTML = resultado;
                break;

        }

        accion = null;
        resultado = 0;
    }

});

signo.addEventListener("click", function() {
    let num = +mostrador.value;
    if (typeof num === 'number') {
        if (num >= 0) {
            mostrador.value = Math.abs(mostrador.value);
        } else {
            mostrador.value = (mostrador.value * -1);
        }
    }
});


punto.addEventListener("click", function() {
    let num = +mostrador.value;
    if (typeof num === 'number') {

        if (num % 1 === 0) {
            pasarNumero('.');
        }
    }
});


function pasarNumero(numero) {
    if (mostrador.value == '0') {
        mostrador.value = numero;

    } else {
        mostrador.value += numero;
    }

    mostrador.innerHTML = mostrador.value;

}