//Palindromo

var frase = "";
var arrayNormal = new Array();
var arrayInverso;

frase = prompt("saber si es un palindromo");
alert(palindromo(frase));



//Funcio palidromo
function palindromo(frase) {
    //frase sin espacios con todas las palabras juntas
    var sinespacio = "";

    //convertir la frase a minuscula
    frase = frase.toLowerCase();

    //cnvertir en un array
    arrayNormal = frase.split("");

    //Eliminar espacios
    for (let i = 0; i < arrayNormal.length; i++) {
        if (arrayNormal[i] != " ") {
            sinespacio += arrayNormal[i];
        }
    }

    //volver a converir en un array
    var nuevoArray = sinespacio.split("");
    arrayInverso = sinespacio.split("").reverse();
    console.log(nuevoArray, arrayInverso);

    //verificar si son iguales recorriendo el array desde inicio y el inverso
    var correcto = true;

    //Comprobar que los dos arrays sean iguales para que sea un palindromo
    for (let i = 0; i < arrayNormal.length; i++) {
        if (nuevoArray[i] != arrayInverso[i]) {
            correcto = false;
            break;
        }
    }

    if (correcto) {
        return "És un palindromo";
    } else {
        return "No és un palindromo";
    }

}