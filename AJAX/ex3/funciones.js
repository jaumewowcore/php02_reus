addEventListener('load',inicialitzarEventos, false);

function inicialitzarEventos(){    
    var ob = document.getElementById('boton1');
    ob.addEventListener('click', presionBoton, false);
    
}

function presionBoton(e){
    var nombre = document.getElementById('nombre');
    var voto = document.getElementById('voto');
    cargarVoto(voto.value, nombre.value);
}

var conexion1;
function cargarVoto(voto, nombre){
    conexion1 = new XMLHttpRequest();
    conexion1.onreadystatechange = procesarEventos;
    conexion1.open("GET", 'pagina1.php?puntaje='+voto+'&nombre='+nombre, true);
    conexion1.send();
}

function procesarEventos(){
    var resultados = document.getElementById("resultados");
    if(conexion1.readyState == 4){
        resultados.innerHTML = conexion1.responseText;
    }else{
        resultados.innerHTML = "Cargando.."
    }
}
