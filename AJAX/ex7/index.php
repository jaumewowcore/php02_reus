<!DOCTYPE html>
<html>
    <head>
        <title>Ejercicio</title>
        <script src="funciones.js"></script>
    </head>
    <body>

        <form id="formulario" action="pagina1.php" method="POST">     
            <p>
                <label>Nombre</label>
                <input type="text" id="nombre" size="20" />        
            </p>

            <p>
                <label>Comentarios</label><br>
                <textarea id="comentarios" name="comentarios" rows="10" cols="50"></textarea>
            </p>

            <input id="enviar" type="submit" value="Enviar" />

            <div id="resultados"></div>
            <br>
            <a href="comentarios.txt">Ver resultados</a>
        </form>
    </body>
</html>