addEventListener('load',inicialitzarEventos, false);

function inicialitzarEventos(){    
    var ref = document.getElementById('formulario');
    ref.addEventListener('submit', enviarDatos, false);
    
}

function enviarDatos(e){
   e.preventDefault();
   enviarFormulario();
}


var conexion1;
function enviarFormulario(){
    conexion1 = new XMLHttpRequest();
    conexion1.onreadystatechange = procesarEventos;
    conexion1.open("POST", 'pagina1.php' , true);
    conexion1.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    conexion1.send(retornarDatos());
}

function procesarEventos(){
    var resultados = document.getElementById("resultados");
    if(conexion1.readyState == 4){
        resultados.innerHTML = "Gracias.";
    }else{
        resultados.innerHTML = "Procesando.."
    }
}

function retornarDatos(){
    var cadena='';
    var nombre = document.getElementById('nombre').value;
    var comentarios = document.getElementById('comentarios').value;
    
    cadena = 'nombre='+encodeURIComponent(nombre)+'&comentarios='+encodeURIComponent(comentarios);
    
    return cadena;
}