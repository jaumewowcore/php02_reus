addEventListener('load',inicialitzarEventos, false);

function inicialitzarEventos(){    
    var ref = document.getElementById('fecha');
    var vec1 = ref.getElementsByTagName("a");
    
    for(i=0; i < vec1.length; i++){
        vec1[i].addEventListener("click", presionEnlace, false);
    }
    
}

function presionEnlace(e){
   e.preventDefault();
   var url = e.target.getAttribute("href");
   verComentarios(url);
}


var conexion1;
function verComentarios(url){
    if(url == ''){
        return;
    }
    conexion1 = new XMLHttpRequest();
    conexion1.onreadystatechange = procesarEventos;
    conexion1.open("GET", url , true);
    conexion1.send();
}

function procesarEventos(){
    var resultados = document.getElementById("comentarios");
    if(conexion1.readyState == 4){
        resultados.innerHTML = conexion1.responseText;
    }else{
        resultados.innerHTML = "Procesando.."
    }
}