addEventListener('load',inicialitzarEventos, false);

function inicialitzarEventos(){    
    var valUl = document.getElementById('votofoto1');
    var vec1 = valUl.getElementsByTagName('li');
    var vec2 = valUl.getElementsByTagName('a');
    
    //Implementar las funciones al clicar o pasar el raton por los links
    for(var i=0; i< vec2.length; i++){
        vec1[i].addEventListener('click', fuera, false);
        vec1[i].addEventListener('mouseover', encima, false);
        vec2[i].addEventListener('mouseout', presionBoton, false);
    }
    
}

//Metodo que pinta el raton esta por encima
function encima(e){
    var ref = e.target;
    var valUl = document.getElementById('votofoto1');
    var vec1 = valUl.getElementsByTagName('li');
    
    for(var i=0; i< ref.firstChild.nodeValue; i++){
        vec1[i].firstChild.style.background='#f00';
        vec1[i].firstChild.style.color='#fff';
    }
}

//Metodo que el raton no esta sobre los links
function fuera(e){
    var ref = e.target;
    var valUl = document.getElementById('votofoto1');
    var vec1 = valUl.getElementsByTagName('li');
    
    for(var i=0; i< ref.firstChild.nodeValue; i++){
        vec1[i].firstChild.style.background='#f7f8e8';
        vec1[i].firstChild.style.color='#f00';
    }
}

function presionBoton(e){
    e.preventDefault();
    var ref = e.target;
    cargarVoto(ref.firstChild.nodeValue);
}

var conexion1;
function cargarVoto(voto){
    conexion1 = new XMLHttpRequest();
    conexion1.onreadystatechange = procesarEventos;
    var aleatorio = Math.random();
    conexion1.open("GET", 'pagina1.php?puntaje='+voto+'&aleatorio='+aleatorio, true);
    conexion1.send();
}

function procesarEventos(){
    var resultados = document.getElementById("resultados");
    if(conexion1.readyState == 4){
        resultados.innerHTML = "Gracias.";
    }else{
        resultados.innerHTML = "Cargando.."
    }
}
