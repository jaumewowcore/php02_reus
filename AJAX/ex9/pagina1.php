<?php

if($_REQUEST['country'] == "Argentina"){
    $superficie = 2700000;
    $capital = "Buenos Aires";
    $idioma = "castellano";
    $poblacion = 3800000;
}

if($_REQUEST['country'] == "Brasil"){
    $superficie = 8500000;
    $capital = "Brasilia";
    $idioma = "portugues";
    $poblacion = 16300000;
}

if($_REQUEST['country'] == "Chile"){
    $superficie = 7500000;
    $capital = "Santiago";
    $idioma = "castellano";
    $poblacion = 15000000;
}

$xml ="<?xml version=\"1.0\"?>\n";
$xml.="<pais>\n";
$xml.="<superficie>$superficie</superficie>\n";
$xml.="<capital>$capital</capital>\n";
$xml.="<idioma>$idioma</idioma>\n";
$xml.="<poblacion>$poblacion</poblacion>\n";
$xml.="</pais>\n";
header("Content-Type: text/xml");

echo $xml;
