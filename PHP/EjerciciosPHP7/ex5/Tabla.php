<?php

class Tabla {

    private $filas;
    private $columnas;
    private $datofila;
    private $datocolumna;
    private $color;
    private $fondo;
    private $valor;

    public function __construct($filas, $columnas) {
        $this->filas = $filas;
        $this->columnas = $columnas;
    }

    public function color($color) {
        $this->color = $color;
    }

    public function fondo($fondo) {
        $this->fondo = $fondo;
    }

    public function colocar($valor, $datofila, $datocolumna) {
        $this->datofila = $datofila;
        $this->datocolumna = $datocolumna;
        $this->valor = $valor;
    }

    public function imprimir() {
        echo "<table border=1 style='width:400px;height:400px;'>";
        for ($i = 0; $i < $this->filas; $i++) {
            echo "<tr>";
            for ($c = 1; $c <= $this->columnas; $c++) {
                if ($i == $this->datofila && $c == $this->datocolumna) {
                    echo "<td style='color:$this->color;background:$this->fondo;'>$this->valor</td>";
                }
                echo "<td></td>";
            }
            echo "</tr>";
        }
        echo '</table>';
    }

}
 