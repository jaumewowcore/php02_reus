<?php

class CabeceraPagina{
    private $titulo;
    private $color = "green";
    private $ubicacion;


    public function __construct($titulo, $color,$ubicacion) {
        $this->titulo = $titulo;
        $this->color = $color;
        $this->ubicacion = $ubicacion;
        
    }
          
    
    public function imprimir(){
        echo "<p style='color:$color;text-align: $ubicacion;'>".$titulo."</p>";
        
    }

}
