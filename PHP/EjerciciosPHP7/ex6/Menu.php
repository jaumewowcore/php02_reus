<?php

class Menu{
    private $opciones = [];   
    
    public function __construct($dades) {
        $this->opciones = $dades;
    }
          
    public function vertical(){
        for($i=0;$i<count($this->opciones); $i++){
            echo $this->opciones[$i];
            echo "<br>";
        }
    }
    
    public function horitzontal(){
        for($i=0;$i<count($this->opciones); $i++){
            echo $this->opciones[$i]." ";
        }
    }
    
    public function mostrar($pos="horitzontal"){
        if($pos == "horitzontal")
            $this->horitzontal();
        else
            $this->vertical();
    }
    

}
