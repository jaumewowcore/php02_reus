<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Ejercicios</title>
    </head>
    <body>
        <?php
            require_once 'Empleado.php';
            $empleado1 = new Empleado("Juan", 800);
            $empleado1->pagar();
            
            echo "<br>";
            echo "<br>";
            
            $empleado2 = new Empleado("Juan");
            $empleado2->pagar();
        ?>
    </body>
</html>

