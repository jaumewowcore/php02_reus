<?php

class Empleado{
    private $nombre;
    private $sueldo;
    
    public function __construct($nombre, $sueldo=1000) {
        $this->nombre = $nombre;
        $this->sueldo = $sueldo;
    }
    
    
    public function getNombre() {
        return $this->nombre;
    }

    public function getSueldo() {
        return $this->sueldo;
    }

    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    public function setSueldo($sueldo) {
        $this->sueldo = $sueldo;
    }
    
    public function pagar(){
        echo $this->getNombre()." tiene un sueldo de ".$this->getSueldo()."<br>";
        if($this->getSueldo() >= 3000)
            echo "Pagar impuestos";
        else
            echo "No debe pagar impuestos";
    }
    

}
