<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Ejercicios</title>
    </head>
    <body>
        <?php
            require_once 'Tabla.php';
            require_once 'Celda.php';
            $tabla = new Tabla(3, 3);  
            $tabla->llenarTabla();
            $tabla->rellenar(1,1,"Mensaje1","red", "yellow");     
            $tabla->rellenar(1,2,"Mensaje2","green", "yellow");     
            $tabla->rellenar(3,3,"Mensaje3","black", "yellow");     
            $tabla->rellenar(2,1,"Mensaje4","black", "yellow");     
            $tabla->crearTabla();
        ?>
    </body>
</html>

