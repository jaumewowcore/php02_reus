<?php

class Celda {

    private $color;
    private $fondo;
    private $valor;

    public function __construct($valor, $color="black", $fondo="white") {
        $this->valor = $valor;
        $this->color = $color;
        $this->fondo = $fondo;
    }

      
    
    public function getColor() {
        return $this->color;
    }

    public function getFondo() {
        return $this->fondo;
    }

    public function getValor() {
        return $this->valor;
    }

    public function setColor($color) {
        $this->color = $color;
    }

    public function setFondo($fondo) {
        $this->fondo = $fondo;
    }

    public function setValor($valor) {
        $this->valor = $valor;
    }



}
 