<?php

require_once 'Celda.php';

class Tabla {

    private $columnas;
    private $filas;
    private $celdas = array();

    public function __construct($filas = 6, $columnas = 6) {
        $this->filas = $filas;
        $this->columnas = $columnas;
    }

    public function rellenar($fila, $columna, $valor = "", $color="", $fondo="") {
        $this->celdas[$fila][$columna] = new Celda($valor, $color, $fondo);
    }

    public function crearTabla() {
        echo "<table border='1' >";
        
        for ($i=1; $i<=$this->filas; $i++) {
            echo "<tr>";  
            
                for ($col=1; $col<=$this->columnas; $col++) {
                    echo $this->crearCelda($i, $col);
                }    
                
            echo "</tr>";
        }
        
        echo "</table>";
    }

    public function llenarTabla() {
        for ($i=1; $i<=$this->filas; $i++) {
            
            for ($col=1; $col<=$this->columnas; $col++) {
                $this->rellenar($i, $col);
            }
            
        }
    }

    public function crearCelda($fila, $columna) {
        $celda = "<td style='background-color:" . $this->celdas[$fila][$columna]->getColor() .
                ";color:" . $this->celdas[$fila][$columna]->getFondo() . ";'>" .
                $this->celdas[$fila][$columna]->getValor() . "</td>";

        return $celda;
    }

}
