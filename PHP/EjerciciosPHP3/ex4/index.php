<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Ejercicio 4</title>
    </head>
    <body>
        <h1>Numeros aleatorios</h1>
        
        <form action="ex4.php" method="post">
            <p>
                <label for="numeros">Cantidad de numeros a devolver</label>
                <input type="number" name="numeros" required>
            </p>
            
            <p>
                <label for="min">Numero mínimo</label>
                <input type="number" name="min" required>
            </p>
            
            <p>
                <label for="max">Numero máximo</label>
                <input type="number" name="max" required>
            </p>
            
            <input type="submit" value="Enviar">
        </form>
    </body>
</html>

