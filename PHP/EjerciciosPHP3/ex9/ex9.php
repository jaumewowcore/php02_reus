<?php

//Comprovar el metodo post por el formulario
if ($_SERVER['REQUEST_METHOD'] === 'POST' &&
        !empty($_POST['numero'])) {

    $numero = $_POST['numero'];

    if (is_numeric($numero)) {
        echo contarCifras($numero);
        
    } else {
        echo "No es un numero";
    }
}

retornar();



function contarCifras($numero) {   
    $cadena = (string) $numero;
    
    return strlen($cadena);
}

//Vuelve a la pagina del formulario
function retornar() {
    header("refresh:4;url=index.php");
}
