<?php

//Comprovar el metodo post por el formulario
if ($_SERVER['REQUEST_METHOD'] === 'POST' &&
        !empty($_POST['minimo']) &&
        !empty($_POST['maximo'])) {

    $minimo = $_POST['minimo'];
    $maximo = $_POST['maximo'];
    static $vector1 = array();
    static $vector2 = array();

    if (is_numeric($minimo) &&
            is_numeric($maximo)) {
        //Rellenar arrays
        generar($minimo, $maximo);
        aleatorio($minimo, $maximo);

        //Mostrar contenido de los Arrays
        mostrar($vector1);
        echo "<br>";
        mostrar($vector2);

        //Multiplicar contenido de array con otro array
        multiplicar($vector1, $vector2);
    }
}

//retornar();


function mostrar($vec) {
    for ($i = 0; $i < sizeof($vec); $i++) {
        echo "<br>En la posicion $i hay el valor " . $vec[$i];
    }
}

function aleatorio($minimo, $maximo) {
    global $vector1;
    global $vector2;
    $res = 0;

    while ($maximo != $minimo && $minimo <= sizeof($vector1)) {
        $minimo++;
        $res = rand($minimo, $maximo);
        array_push($vector2, $res);
    }
}

function generar($minimo, $maximo) {
    global $vector1;

    while ($maximo != $minimo) {
        if (primos($minimo)) {
            array_push($vector1, $minimo);
        }
        $minimo++;
    }
}

function primos($numero) {
    $primo = true;

    for ($i = 2; $i < $numero; $i++) {
        if ($numero % $i == 0) {
            $primo = false;
        }
    }

    return $primo;
}

function multiplicar($arr1, $arr2) {
    $calculos = 0;

    for ($i = 0; $i < sizeof($arr1); $i++) {
        $calculos = $arr1[$i] * $arr2[$i];
        echo "<br><br>En la posicion $i la multiplicacion es igual a $calculos";
    }
}

//Vuelve a la pagina del formulario
function retornar() {
    header("refresh:4;url=index.php");
}
