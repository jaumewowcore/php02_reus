<?php

session_start();

if (!isset($_SESSION['intentos'])) {
    $_SESSION['intentos'] = 0;
}

if (isset($_SESSION['intentos'])) {
    if (isset($_SESSION['intentos']) >= 2) {
        echo "<br>Cuenta bloqueada";
        $_SESSION['intentos'] = 0;
    }
}


static $contrasena = "hola";
static $usuario = "jaume";

//Comprovar el metodo post por el formulario
if ($_SERVER['REQUEST_METHOD'] === 'POST' &&
        !empty($_POST['contra']) &&
        !empty($_POST['contra'])) {


    $user = $_POST['usuario'];
    $contra = $_POST['contra'];

    identificarse($user, $contra);
}

function identificarse($user, $contra) {
    global $usuario;
    global $contrasena;

    if ($usuario == $user) {

        if ($contra == $contrasena && $_SESSION['intentos'] <= 2) {
            return "Enhorabuena";
            
        } else {
            $_SESSION['intentos']++;
            echo "La contraseña no coincide";
            retornar();
        }
        
    } else {
        echo "Nombre inexistente";
        retornar();
    }
}

//En caso que no se pase el numero vuelve al formulario
function retornar() {
    header("refresh:4;url=index.php");
}
