<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Ejercicio 5</title>
    </head>
    <body>
        <h1>Calcular area</h1>

        <form action="ex5.php" method="post">
            <p>
                <label for="numero1">Numero1</label>
                <input type="number" name="numero1" required>
            </p>

            <p>
                <label for="numero2">Numero2</label>
                <input type="number" name="numero2" placeholder="Para el triangulo">
            </p>

            <p>
                <select name="figura">
                    <option value="1">Circulo</option>
                    <option value="2">Triangulo</option>
                    <option value="3">Cuadrado</option>
                </select>
            </p>

            <input type="submit" value="Enviar">
        </form>
    </body>
</html>

