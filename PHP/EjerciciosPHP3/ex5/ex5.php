<?php
//Comprovar el metodo post por el formulario
if ($_SERVER['REQUEST_METHOD'] === 'POST' &&
        !empty($_POST['numero1']) &&
        !empty($_POST['figura'])) {


    $numero1 = $_POST['numero1'];
    $numero2 = $_POST['numero2'];
    $figura = $_POST['figura'];
    
    if(is_numeric($figura) && 
       is_numeric($numero1) ){
        
        switch ($figura) {
            case 1:
                echo circulo($numero1);
                echo '';
                break;

            case 2:
                echo triangulo($numero1, $numero2);
                break;

            case 3:
                echo cuadrado($numero1);
                break;
        }
        
    }
   
        
}
    retornar();

function circulo($radio){
    return pow($radio, 2) * pi();
}

function triangulo($base, $altura){
    return ($base * $altura) / 2;
}

function cuadrado($lado){
    return ($lado * $lado);
}

//Vuelve a la pagina del formulario
function retornar() {
    header("refresh:4;url=index.php");
}
