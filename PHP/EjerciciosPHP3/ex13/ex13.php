<?php

//Comprovar el metodo post por el formulario
if ($_SERVER['REQUEST_METHOD'] === 'POST' &&
        !empty($_POST['minimo']) &&
        !empty($_POST['maximo'])) {

    $minimo = $_POST['minimo'];
    $maximo = $_POST['maximo'];
    static $vector = array();

    if (is_numeric($minimo) &&
        is_numeric($maximo)) {
        generar($minimo, $maximo);
        mostrar();
    }
}

//retornar();



function mostrar() {
    global $vector;
    $suma = 0;

    for ($i = 0; $i < sizeof($vector); $i++) {
        echo "<br>En la posicion $i hay el valor " . $vector[$i];
        $suma += $vector[$i];
    }
    
    echo "<br><br>La suma total de los primos es $suma";
}

function generar($minimo, $maximo) {
    global $vector;

    while ($maximo != $minimo) {
        if (primos($minimo)) {
            array_push($vector, $minimo);
        }
        $minimo++;
    }
}

function primos($numero) {
    $primo = true;

    for ($i = 2; $i < $numero; $i++) {
        if ($numero % $i == 0) {
            $primo = false;
        }
    }

    return $primo;
}

//Vuelve a la pagina del formulario
function retornar() {
    header("refresh:4;url=index.php");
}
