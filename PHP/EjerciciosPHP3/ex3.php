<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Ejercicio 3</title>
    </head>
    <body>
        <h1>Vector asociativo</h1>
        <?php
            $usuarios = array(
                "jaume" => "jaume1234",
                "pere" => "pere123",
                "joan" => "joan132ds",
                "carles" => "carlosfsk",
                "noel" => "noelks"                
            );
            
            echo "La clave de carles es ".$usuarios['carles'];
        ?>
    </body>
</html>

