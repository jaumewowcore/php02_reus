<?php

//Comprovar el metodo post por el formulario
if ($_SERVER['REQUEST_METHOD'] === 'POST' &&
        !empty($_POST['numero'])) {

    $numero = $_POST['numero'];

    if (is_numeric($numero)) {
        echo numeroBinario($numero);        
    }else{
        echo "No es un numero decimal";
    }
}

retornar();



function numeroBinario($numero) {
    $resto = "";

    while ($numero != 0) {
        $resto .= $numero%2;
        $numero = intval($numero/2);
    }

    return $resto;
}

//Vuelve a la pagina del formulario
    function retornar() {
        header("refresh:4;url=index.php");
    }
    