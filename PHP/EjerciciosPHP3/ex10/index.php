<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Ejercicio 10</title>
    </head>
    <body>
        <h1>Contar las cifras del numero</h1>

        <form action="ex10.php" method="post">
            <p>
                <label for="numero">Numero</label>
                <input name="numero" required>
            </p>
            
            <p>
               <select name="moneda">
                   <option value="1" selected>Libras</option>
                   <option value="2">Dolares</option>
                   <option value="3">Yenes</option>
               </select>
           </p>

            <input type="submit" value="Enviar">
        </form>
    </body>
</html>

