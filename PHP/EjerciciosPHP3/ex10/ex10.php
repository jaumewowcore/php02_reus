<?php

//Comprovar el metodo post por el formulario
if ($_SERVER['REQUEST_METHOD'] === 'POST' &&
    !empty($_POST['moneda']) &&
    !empty($_POST['numero'])) {

    $numero = $_POST['numero'];
    $moneda = $_POST['moneda'];

    
    echo convertir($numero, $moneda);
        
   
}

retornar();



function convertir($numero, $moneda) {   
    $resultado = 0;
    switch ($moneda) {
        case 1:
            $resultado = $numero * 0.86;
            break;
        
        case 2:
            $resultado = $numero * 1.28611;
            break;
        
        case 3:
            $resultado = $numero * 129.852;
            break;

        
    }
    
    return $resultado;
}

//Vuelve a la pagina del formulario
function retornar() {
    header("refresh:4;url=index.php");
}
