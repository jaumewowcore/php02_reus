<?php

//Comprovar el metodo post por el formulario
if ($_SERVER['REQUEST_METHOD'] === 'POST' &&
    !empty ($_POST['minimo']) &&
    !empty ($_POST['maximo']) ){
    
    $minimo = $_POST['minimo'];
    $maximo = $_POST['maximo'];
    static $vector = array();
    
    if(is_numeric($_POST['minimo']) &&
        is_numeric($_POST['maximo']) ){
        generar($minimo, $maximo);
        mostrar();
    }
        
   
}

//retornar();



function mostrar() {  
    global $vector;
    $suma = 0;
    
    for($i=0; $i< sizeof($vector); $i++){
        echo "<br>En la posicion $i hay el valor ".$vector[$i];
        $suma += $vector[$i]; 
    }
}

function generar($min, $max){
    global $vector;
    $numAl = 0;
    
    if($min > $max){
        $max = 9;
        $min = 0;
    }
    
    for($i=0;$i<=9;$i++){
        $numAl = rand($min, $max);
        array_push($vector, $numAl);
    }    
    
    
}

//Vuelve a la pagina del formulario
function retornar() {
    header("refresh:4;url=index.php");
}
