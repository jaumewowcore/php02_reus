<?php

// Modelo
require_once 'models/Rubro.php';

class RubroController {

    public function index() {
        header("Location: index.php?controller=Rubro&action=crear");
    }
    
    public function crear(){
        //Vista
        require_once 'views/rubros/crear.php';
    }

    public function guardar() {
        if(isset($_REQUEST['descripcion']) && !empty($_REQUEST)){
            $desc = $_REQUEST['descripcion'];
            $rubro = new Rubro();
            $rubro->setDescripcion($desc);
            
            if(!$rubro->getOne()){
                $rubro->guardar();
            }
        }
       
        header("Location: index.php?controller=Rubro&action=crear");
    }

}
