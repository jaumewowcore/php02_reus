<?php

// Modelos
require_once 'models/Articulo.php';
require_once 'models/Rubro.php';

class ArticuloController {

    public function index() {
        header("Location: index.php?controller=articulo&action=listar");
    }

    public function crear() {
        //Coger datos de los rubros        
        $rubro = new Rubro();
        $rubros = $rubro->getAll();

        if (is_object($rubros)) {
            //Vista
            require_once 'views/articulos/crear.php';
        }
    }

    public function listar() {
        //Coger datos de los rubros i articulos       
        $articulos = new Articulo();
        $articulos = $articulos->getAllAndRubro();

        if (is_object($articulos)) {
            //Vista
            require_once 'views/articulos/listar.php';
        }
    }

    public function guardar() {
        if (isset($_REQUEST['descripcion']) && !empty($_REQUEST['descripcion']) &&
                isset($_REQUEST['precio']) && is_numeric($_REQUEST['precio']) &&
                isset($_REQUEST['rubro']) && is_numeric($_REQUEST['rubro'])) {

            $desc = $_REQUEST['descripcion'];
            $precio = $_REQUEST['precio'];
            $rubro = $_REQUEST['rubro'];

            $articulo = new Articulo();
            $articulo->setDescripcion($desc);
            $articulo->setPrecio($precio);
            $articulo->setCodigorubro($rubro);

            $articulo->guardar();
        }

        header("Location: index.php?controller=articulo&action=crear");
    }

}
