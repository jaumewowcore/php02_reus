<?php

// Modelos
require_once 'models/Articulo.php';
require_once 'models/Rubro.php';

class ArticuloController {

    public function index() {
        header("Location: index.php?controller=articulo&action=tabla");
    }

    public function buscarDestroy() {
    //Vista
        require_once 'views/articulos/buscar.php';
    }

    public function buscar() {
    //Vista
        require_once 'views/articulos/buscar.php';
    }

    public function tabla() {
        $articulo = new Articulo();
        $articulos = $articulo->getAllAndRubro();
        //Vista
        require_once 'views/articulos/tabla.php';
    }

    //Carga los datos del articulo encontrado para modificar
    public function cargar() {
        if (isset($_REQUEST['id']) && is_numeric($_REQUEST['id'])) {
            $codigo = $_REQUEST['id'];
            $articulo = new Articulo();
            $articulo->setCodigo($codigo);

    //Comprobar si el articulo existe
            if ($articulo->getOne()) {
                $datos = $articulo->getArticulo();
    //Cargar todas las rubros
                $rubro = new Rubro();
                $rubros = $rubro->getAll();

    //Cargar la vista actualizar
                require_once 'views/articulos/actualizar.php';
            } else {
                header("Location: index.php?controller=articulo&action=buscar");
            }
        }
    }

    public function modificar() {
        if (isset($_REQUEST['descripcion']) && !empty($_REQUEST['descripcion']) &&
                isset($_REQUEST['precio']) && is_numeric($_REQUEST['precio']) &&
                isset($_REQUEST['id']) && is_numeric($_REQUEST['id']) &&
                isset($_REQUEST['rubro']) && is_numeric($_REQUEST['rubro'])) {

            $codigo = +$_REQUEST['id'];
            $desc = $_REQUEST['descripcion'];
            $precio = $_REQUEST['precio'];
            $rubro = +$_REQUEST['rubro'];

            $articulo = new Articulo();
            $articulo->setCodigo($codigo);
            $articulo->setDescripcion($desc);
            $articulo->setPrecio($precio);
            $articulo->setCodigorubro($rubro);

            $articulo->modificar();
        }

        header("Location: index.php?controller=articulo&action=buscar");
    }

    public function crear() {
        //Coger datos de los rubros        
        $rubro = new Rubro();
        $rubros = $rubro->getAll();

        if (is_object($rubros)) {
        //Vista
            require_once 'views/articulos/crear.php';
        }
    }

    public function listar() {
        //Coger datos de los rubros i articulos       
        $articulos = new Articulo();
        $articulos = $articulos->getAllAndRubro();

        if (is_object($articulos)) {
            //Vista
            require_once 'views/articulos/listar.php';
        }
    }
    
    public function aumentar() {
        //Coger datos de los rubros i articulos       
        $articulos = new Articulo();
        $articulos->aumentar();  
    }

    public function guardar() {
        if (isset($_REQUEST['descripcion']) && !empty($_REQUEST['descripcion']) &&
                isset($_REQUEST['precio']) && is_numeric($_REQUEST['precio']) &&
                isset($_REQUEST['rubro']) && is_numeric($_REQUEST['rubro'])) {

            $desc = $_REQUEST['descripcion'];
            $precio = $_REQUEST['precio'];
            $rubro = $_REQUEST['rubro'];

            $articulo = new Articulo();
            $articulo->setDescripcion($desc);
            $articulo->setPrecio($precio);
            $articulo->setCodigorubro($rubro);

            $articulo->guardar();
        }

    }

    public function destroy() {
        if (isset($_REQUEST['id']) && is_numeric($_REQUEST['id'])) {
            $codigo = $_REQUEST['id'];
            $articulo = new Articulo();
            $articulo->setCodigo($codigo);

            if ($articulo->getOne()) {
                $articulo->destroy();
                
            } else {
                echo "No existe el ID $codigo";
            }
        }
    }

}
