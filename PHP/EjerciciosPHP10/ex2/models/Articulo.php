<?php

require_once 'ModeloBase.php';

class Articulo extends ModeloBase {

    public $codigo;
    public $descripcion;
    public $precio;
    public $codigorubro;

    public function __construct() {
        parent::__construct();
    }

    public function getCodigo() {
        return $this->codigo;
    }

    public function getDescripcion() {
        return $this->descripcion;
    }

    public function getPrecio() {
        return $this->precio;
    }

    public function getCodigorubro() {
        return $this->codigorubro;
    }

    public function setCodigo($codigo) {
        $this->codigo = $codigo;
    }

    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    public function setPrecio($precio) {
        $this->precio = $precio;
    }

    public function setCodigorubro($codigorubro) {
        $this->codigorubro = $codigorubro;
    }


    public function guardar() {
        $sql = "INSERT INTO articulos(descripcion, precio, codigorubro) "
        . "VALUES ('{$this->getDescripcion()}', "
        . "{$this->getPrecio()}, {$this->getCodigorubro()} )";

        $guardado = $this->db->query($sql);

        return $guardado;
    }

}
