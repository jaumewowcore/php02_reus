CREATE DATABASE base2;
use base2;

CREATE TABLE rubros(
codigo      int(255) auto_increment not null,
descripcion MEDIUMTEXT not null,
CONSTRAINT pk_codigo PRIMARY KEY(codigo)
)ENGINE=InnoDb;

CREATE TABLE articulos(
codigo          int(255) auto_increment not null,
descripcion     MEDIUMTEXT not null,
precio          double(255,2) UNSIGNED not null,
codigorubro     int(255) not null,
PRIMARY KEY(codigo),
FOREIGN KEY(codigorubro) REFERENCES rubros(codigo)
)ENGINE=InnoDb;

INSERT INTO rubros (descripcion) VALUES ('frutas');
INSERT INTO rubros (descripcion) VALUES ('verduras');
INSERT INTO rubros (descripcion) VALUES ('carnes');