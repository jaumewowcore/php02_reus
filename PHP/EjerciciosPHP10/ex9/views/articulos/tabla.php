
<h2>Articulos</h2>
<table>
    <tr>
        <th>Codigo</th>
        <th>Descripción</th>
        <th>Precio</th>
        <th>Rubro</th>
        <th>Borrar</th>
        <th>Modificar</th>
    </tr>
    <?php foreach ($articulos as $articulo) { ?>
        <tr>
            <td><?= $articulo['codigo'] ?></td>
            <td><?= $articulo['descripcion'] ?></td>
            <td><?= $articulo['precio'] ?></td>
            <td><?= $articulo['rubros'] ?></td>
            <td><a href="index.php?controller=articulo&action=destroy&id=<?=$articulo['codigo'] ?>">Borra?</a></td>
            <td><a href="index.php?controller=articulo&action=cargar&id=<?=$articulo['codigo'] ?>">Modifica?</a></td>
        </tr>
    <?php } ?>
</table>
<br>
<a href="index.php?controller=articulo&action=crear">Agrega un nuevo articulo</a>