<?php

require_once 'ModeloBase.php';

class Articulo extends ModeloBase {

    public $codigo;
    public $descripcion;
    public $precio;
    public $codigorubro;

    public function __construct() {
        parent::__construct();
    }

    public function getCodigo() {
        return $this->codigo;
    }

    public function getDescripcion() {
        return $this->descripcion;
    }

    public function getPrecio() {
        return $this->precio;
    }

    public function getCodigorubro() {
        return $this->codigorubro;
    }

    public function setCodigo($codigo) {
        $this->codigo = $codigo;
    }

    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    public function setPrecio($precio) {
        $this->precio = $precio;
    }

    public function setCodigorubro($codigorubro) {
        $this->codigorubro = $codigorubro;
    }

    public function getAllAndRubro() {
        $sql = "SELECT a.*, r.descripcion AS rubros
                FROM articulos a
                INNER JOIN rubros r
                ON a.codigorubro = r.codigo";

        $guardado = $this->db->query($sql);

        return $guardado;
    }

    //Comprueba si existe el articulo
    public function getOne() {
        $sql = "SELECT * FROM articulos WHERE codigo = '{$this->getCodigo()}'";

        $guardado = $this->db->query($sql);

        if ($registro = mysqli_fetch_array($guardado)) {
            return true;
        } else {
            return false;
        }
    }

    //Devuelve los datos del articulo
    public function getArticulo() {
        $sql = "SELECT * FROM articulos WHERE codigo = '{$this->getCodigo()}'";

        $guardado = $this->db->query($sql);

        $registro = mysqli_fetch_array($guardado);
        return $registro;
    }

    public function aumentar() {
        $sql = "Update articulos SET precio=precio*1.1 WHERE precio <= 5";
        var_dump($sql);die();

        $guardado = $this->db->query($sql);
        
        printf("Affected rows (SELECT): %d\n", mysqli_affected_rows($this->db));
    }

    public function destroy() {
        $sql = "Delete FROM articulos WHERE codigo = '{$this->getCodigo()}'";

        $guardado = $this->db->query($sql);

        printf("Affected rows (SELECT): %d\n", mysqli_affected_rows($this->db));
    }

    public function modificar() {
        $sql = "UPDATE articulos SET descripcion='{$this->getDescripcion()}', "
                . "precio={$this->getPrecio()}, codigorubro={$this->getCodigorubro()} "
                . "WHERE codigo={$this->getCodigo()}";


        $guardado = $this->db->query($sql);

        return $guardado;
    }

    public function guardar() {
        $sql = "INSERT INTO articulos(descripcion, precio, codigorubro) "
                . "VALUES ('{$this->getDescripcion()}', "
                . "{$this->getPrecio()}, {$this->getCodigorubro()} )";

        $guardado = $this->db->query($sql);

        return $guardado;
    }

}
