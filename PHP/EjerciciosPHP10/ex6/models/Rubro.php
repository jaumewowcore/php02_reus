<?php

require_once 'ModeloBase.php';

class Rubro extends ModeloBase {

    public $codigo;
    public $descripcion;

    public function __construct() {
        parent::__construct();
    }

    public function getCodigo() {
        return $this->codigo;
    }

    public function getDescripcion() {
        return $this->descripcion;
    }

    public function setCodigo($codigo) {
        $this->codigo = $codigo;
    }

    public function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    public function getFindOne() {
        $sql = "SELECT * FROM rubros WHERE codigo = '{$this->getCodigo()}'";

        $guardado = $this->db->query($sql);

        return $guardado;
    }

    public function getOne() {
        $sql = "SELECT * FROM rubros WHERE descripcion = '{$this->getDescripcion()}'";

        $guardado = $this->db->query($sql);

        if ($registro = mysqli_fetch_array($guardado)) {
            return true;
        } else {
            return false;
        }
    }

    public function guardar() {
        $sql = "INSERT INTO rubros(descripcion) VALUES ('{$this->getDescripcion()}')";

        $guardado = $this->db->query($sql);

        return $guardado;
    }

}
