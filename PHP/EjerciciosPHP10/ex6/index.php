<h1>Ejercicios con conexion a la base de datos</h1>
<?php
require_once 'autoload.php';

if(isset($_GET['controller'])){
	$nombre_controlador = $_GET['controller'].'Controller';
}else{
    header("Location: index.php?controller=articulo");exit();
}

if(class_exists($nombre_controlador)){	
	$controlador = new $nombre_controlador();
	
	if(isset($_GET['action']) && method_exists($controlador, $_GET['action'])){
		$action = $_GET['action'];
		$controlador->$action();
	}else{
            try {
                $controlador->index();
            } catch (Exception $exc) {
                echo "La pagina que buscas no existe";
                
            }
	}
        
}else{
	echo "La pagina que buscas no existe";
}

