<?php
require_once 'sesion.php';

if (!empty($_GET['galleta']) && isset($_GET['galleta'])) {

    switch ($_GET['galleta']) {
        case 1:
            if (isset($_SESSION['galleta'])) {
                unset($_SESSION['galleta']);
                header("Location: index.php");
            }
            break;
    }
}

if (!empty($_REQUEST['texto']) && isset($_REQUEST['texto'])) {
    $texto = $_REQUEST['texto'];
    $_SESSION['galleta'] = $texto;
}

if (isset($_SESSION['galleta'])) {
    echo "Texto en la sesion es " . $_SESSION['galleta'];
}
?>

<a href="index.php">Volver</a>