<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Ejercicio 14</title>
    </head>
    <body>
        <h1 align="center">VOTAR UNA OPCIÓN</h1>

        <?php
        require_once 'sesion.php';
        
        if(!isset($_SESSION['blue']) || !isset($_SESSION['orange']) ){
            $_SESSION['blue'] = 0;
            $_SESSION['orange'] = 0;
        }
        ?>



        <br><br>
        Haga clic en los botones para modificar el valor
 
        <form action="ex14.php" method="POST">
            <p>
                <button type="submit" name="accion" value="1" style="font-size: 50px; color: blue;">✔</button>
                <div style="background: blue; width: <?=$_SESSION['blue']?>px; height: 50px;">
            </p>
            
            <br><br><br>
            
            <p>
                <button type="submit" name="accion" value="2" style="font-size: 50px; color: orange;">✔</button>
                <div style="background: orange; width: <?=$_SESSION['orange']?>px; height: 50px;">
            </p>
            
            <br><br><br><br>
            
            <button type="submit" name="accion" value="3" style="width: 120px;">Poner a cero</button>
        </form>
    </body>
</html>

