<?php
require_once 'sesion.php';

if (!empty($_GET['galleta']) && isset($_GET['galleta'])) {

    switch ($_GET['galleta']) {
        case 1:
            if (isset($_SESSION['galleta']) && isset($_SESSION['galleta1'])) {
                unset($_SESSION['galleta']);
                unset($_SESSION['galleta1']);
            }
            break;
    }
}

//Mayusculas
if (!empty($_REQUEST['texto']) && isset($_REQUEST['texto'])) {
    $texto = $_REQUEST['texto'];
    
    if($texto == strtoupper($texto)){
        $_SESSION['galleta'] = $texto;        
    }else{
        $_SESSION['galleta'] = "incorrecta";        
        
    }
}

//Minusculas
if (!empty($_REQUEST['texto1']) && isset($_REQUEST['texto1'])) {
    $texto = $_REQUEST['texto1'];
    
    if($texto == strtolower($texto)){
        $_SESSION['galleta1'] = $texto;        
    }else{
        $_SESSION['galleta1'] = "incorrecta";        
        
    }
}

header("Location: index.php");

?>

