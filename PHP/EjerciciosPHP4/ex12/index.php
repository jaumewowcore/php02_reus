<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Ejercicio 12</title>
    </head>
    <body>
        <h1 align="center">Formulario palabra en mayúsculas y en minúsculas</h1>

        <?php
        require_once 'sesion.php';
        
        if (isset($_SESSION['galleta'])) {
            echo "Palabra en majúsculas es ".$_SESSION['galleta'];            
        }
        
        if (isset($_SESSION['galleta1'])) {
            echo "<br>Palabra en minúsculas es ".$_SESSION['galleta1'];            
        }
        ?>
        
        <br><br>
        Escriba una palabra en mayúsculas y otra en minúsculas:
        <form action="ex12.php" method="POST">
            <p>
                <label><strong>Majúsculas:</strong></label>
                <input type="text" name="texto" />
            </p>
            
            <p>
                <label><strong>Minúsculas:</strong></label>
                <input type="text" name="texto1" />
            </p>
            <input type="submit" value="Comprobar" />
            
            <button><a href="ex12.php?galleta=1">Borrar</a></button>
        </form>
        



    </body>
</html>

