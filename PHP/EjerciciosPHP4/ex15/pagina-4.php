<?php

require_once 'sesion.php';

if (isset($_POST['accion']) && !empty($_POST['accion'])) {
    switch ($_POST['accion']) {
        case 1:
            if (isset($_POST['apellidos']) && !empty($_POST['apellidos'])) {
                $_SESSION['apellidos'] = $_POST['apellidos'];
                header("Location: pagina-5.php");
            }
            break;

        case 2:
            if (isset($_SESSION['apellidos'])) {
                unset($_SESSION['apellidos']);
            }
            header("Location: pagina-3.php");
            break;
    }
} else {
    header("Location: pagina-3.php");
}