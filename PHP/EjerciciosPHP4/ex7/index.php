<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Ejercicio 7</title>
    </head>
    <body>

        <h1 align="center">CREACIÓN Y DESTRUCCIÓN DE COOKIES</h1>

        <p>
            Elije una opción
        </p>

        <form action="ex7.php" method="POST">
            <ul>
                <li>
                    Crear una cookie con una duración de
                    <input type="number" name="duracion" min="1" max="60"/>
                    segundos (entre 1 y 60)
                    <input type="submit" value="Crear" />
                </li>
            </ul>
        </form>

        <ul>
            <li>
                Comprobar la cookie                    
                <a href="ex7.php?accion=1">Comprobar</a>
            </li>
        </ul>
        
        <ul>
            <li>
                Destruir la cookie                    
                <a href="ex7.php?accion=2">Destruir</a>
            </li>
        </ul>


    </body>
</html>

