<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Ejercicio 1</title>
    </head>
    <body>
        <?php
        require_once 'sesion.php';
        
        if (isset($_SESSION['message'])) {
            echo "<h2>Inicio de sesion incorrecto</h2>";
            unset($_SESSION['message']);
        }
        ?>
        <form action="verif_login.php" method="post">
            <p>
                <label for="name">Nombre</label>
                <input type="text" name="name" />
            </p>

            <p>
                <label for="passw">Contraseña</label>
                <input type="password" name="passw" />
            </p>

            <input type="submit" value="Login">
        </form>
    </body>
</html>

