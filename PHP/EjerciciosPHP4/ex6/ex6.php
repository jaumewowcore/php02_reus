<?php

if ($_SERVER['REQUEST_METHOD'] === 'GET' &&
        !empty($_GET['color'])) {

    switch ($_GET['color']) {
        case "red":
            setcookie("color", "red", time() + 3000);
            echo "Color seleccionado es rojo";
            break;

        case "blue":
            setcookie("color", "blue", time() + 3000);
            echo "Color seleccionado es azul";
            break;

        case "green":
            setcookie("color", "green", time() + 3000);
            echo "Color seleccionado es verde";

            break;

        case "black":
            setcookie("color", "black", time() - 2);
            echo "No se ha elegido ningun color";

            break;
    }
} else {
    echo "No se ha elegido ningun color";
}

?>
<br><br>
<a href="index.php">Volver a la pagina principal</a>