<?php

require_once 'sesion.php';

//Comprovar el metodo GET por el formulario
if ($_SERVER['REQUEST_METHOD'] === 'GET' &&
        !empty($_GET['altura']) &&
        !empty($_GET['diametro'])) {

    $altura = $_GET['altura'];
    $diametro = $_GET['diametro'];


    if (is_numeric($altura) &&
            is_numeric($diametro)) {

        $_SESSION['altura'] = $altura;
        $_SESSION['diametro'] = $diametro;

        $resultado = pi() * pow($diametro, 2) * $altura;

        echo "El volumen es $resultado";
        
        header("refresh:4;url=index.php");
    }
}