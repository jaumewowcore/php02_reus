<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Ejercicio 10</title>
    </head>
    <body>
        <h1 align="center">Formulario TEXTO(1)</h1>

        <?php
        require_once 'sesion.php';
        
        if (isset($_SESSION['galleta'])) {
            echo "Ultimo texto escrito es ".$_SESSION['galleta'];            
        }
        ?>
        
        <br><br>
        Escriba texto:
        <form action="ex10.php" method="POST">
            <p>
                <label><strong>Texto:</strong></label>
                <input type="text" name="texto" />
            </p>
            <input type="submit" value="Siguiente" />
            
            <button><a href="ex10.php?galleta=1">Borrar</a></button>
        </form>
        



    </body>
</html>

