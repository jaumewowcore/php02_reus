<?php
$nombre_fichero = 'resultados/sumas.txt';

//Comprovar si se pasan los datos
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if(isset($_REQUEST['num1']) && !empty($_REQUEST['num1']) 
    && isset($_REQUEST['num2']) && !empty($_REQUEST['num2']) ){
        $n1 = $_REQUEST['num1'];
        $n2 = $_REQUEST['num2'];
        
        $resultado = ($n1 + $n2);
        
        $texto = "$n1 + $n2 = $resultado\n";      
        //ESCRIBIR                
        $archivo = fopen($nombre_fichero, "a+") or die("Unable to open file!");
        fwrite($archivo, $texto);
        fclose($archivo);

        
    }
}

//Canviar a la pagina de index
header("Location: index.php");


