<?php

//Comprovar si se pasan los datos
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    
    if (isset($_FILES['arxivo1'])) {
        $archivo = $_FILES['arxivo1'];
        subir($archivo);
    }
    
    if (isset($_FILES['arxivo2'])) {
        $archivo = $_FILES['arxivo2'];
        subir($archivo);
    }
    
    if (isset($_FILES['arxivo3'])) {
        $archivo = $_FILES['arxivo3'];
        subir($archivo);
    }
}

function subir($archivo) {
    $nombre = $archivo['name'] . time();

    //Subir al directorio uploads desde el temporal
    move_uploaded_file($archivo['tmp_name'], './uploads/' . $nombre);
}

//Canviar a la pagina de index
header("Location: index.php");


