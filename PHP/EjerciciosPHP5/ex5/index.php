<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Ejercicio 5</title>
    </head>
    <body>
        <form action="ex5.php" method="POST" style="text-align: center;">
            <p>
                <label>Numero a buscar:</label>
                <input type="number" name="buscar" min="1" max="10"/>
                <input type="submit" value="Buscar" />
            </p>   
            
            <?php
            for ($i = 1; $i <= 10; $i++) {
                ?>
            <br>
            <p>
                <label>Nombre:</label>
                <input type="text" name="nombre<?=$i?>" />
            </p>

            <p>
                <label>Edad:</label>
                <input type="number" name="edad<?=$i?>" />
            </p>

            <p>
                <label>Ciudad de residencia:</label>
                <input type="text" name="ciudad<?=$i?>" />
            </p>

            <?php } ?>
            
            <input type="submit" value="Enviar" />

        </form>      
    </body>
</html>

