<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Ejercicio 1</title>
    </head>
    <body>
        <form action="ex2.php" method="POST">
            <p>
                <label>Nombre:</label>
                <input type="text" name="nombre" />
            </p>
            
            <p>
                <label>Dirección:</label>
                <input type="text" name="direccion" />
            </p>
            
            <p>
                <label>Jamon y Queso:</label>
                <input type="checkbox" name="jamon" />
                <input type="number" name="njamon" />
            </p>
            
            <p>
                <label>Napolitana:</label>
                <input type="checkbox" name="napolitana" />
                <input type="number" name="nnapolitana" />
            </p>
            
            <p>
                <label>Muzzarella:</label>
                <input type="checkbox" name="muzzarella" />
                <input type="number" name="nmuzarella" />
            </p>
            
            <input type="submit" value="Confirmar" />            
        </form>   
        <br>
        <a href="ex2.php?mostrar=1">Mostrar</a>
    </body>
</html>

