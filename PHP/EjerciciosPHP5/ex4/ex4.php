<?php

$nombre_fichero = 'gente.txt';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (isset($_REQUEST['nombre']) && !empty($_REQUEST['nombre']) &&
            isset($_REQUEST['edad']) && !empty($_REQUEST['edad']) &&
            isset($_REQUEST['ciudad']) && !empty($_REQUEST['ciudad']) ){
        $texto = "Nombre: " . $_REQUEST['nombre'] . "\n";
        $texto .= "Edad: " . $_REQUEST['edad'] . "\n";
        $texto .= "Ciudad: " . $_REQUEST['ciudad'] . "\n";
        $texto .= ".........\n";

        $archivo = fopen($nombre_fichero, "a+") or die("Unable to open file!");
        fwrite($archivo, $texto);
        fclose($archivo);
    }

    $archivo = fopen($nombre_fichero, "r") or die("Unable to open file!");

    while (!feof($archivo)) {
        echo fgets($archivo) . "<br>";
    }
    fclose($archivo);
}


