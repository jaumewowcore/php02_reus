<?php

//Comprovar el metodo post por el formulario i si el nombre no está vacio
if ($_SERVER['REQUEST_METHOD'] === 'POST' && !empty($_POST['numero1']) && !empty($_POST['numero2'])) {

    $numero1 = $_POST['numero1'];
    $numero2 = $_POST['numero2'];

    //Comprueba si es mayor o menor
    if (is_numeric($numero1) && is_numeric($numero2)) {
        if ($numero1 > $numero2) {
            echo "$numero1 es más grande";
            
        } elseif ($numero1 == $numero2) {
            echo "$numero1 i $numero2 són iguales";
            
        } else {
            echo "$numero2 es más grande";
        }
        
    }else{
       retornar();
    }
    
} else {
    retornar();
}

//En caso que no se pase el contrato vulve al formulario
function retornar() {
    echo "Valores erroneos";
    header("refresh:10;url=form.php");
}
