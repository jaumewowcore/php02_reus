<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Ejercicio 6</title>
    </head>
    <body>
        <h1>Ganancias</h1>
        
        <form action="ex6.php" method="post">
            <p>
                <label for="name">Nombre</label>
                <input type="text" name="name" required>
            </p>

            <p>
                <select name="ganancias">
                    <option value="1" selected>1-1000</option>
                    <option value="2">1001-3000</option>
                    <option value="3">>3000</option>
                </select>
            </p>
            <input type="submit" value="Enviar">
        </form>
    </body>
</html>

