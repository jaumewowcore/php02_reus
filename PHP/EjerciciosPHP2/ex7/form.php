<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Ejercicio 7</title>
    </head>
    <body>
        <h1>Contrato</h1>

        <form action="ex7.php" method="post">
            <p>
                <label for="contrato">Contrato</label>
                <br>
                <textarea name="contrato" required rows="15" cols="60">
En la ciudad de [........], se acuerda entre la Empresa [..........]
representada por el Sr. [..............] en su carácter de Apoderado,
con domicilio en la calle [..............] y el Sr. [..............],
futuro empleado con domicilio en [..............], celebrar el presente
contrato a Plazo Fijo, de acuerdo a la normativa vigente de los
artículos 90,92,93,94, 95 y concordantes de la Ley de Contrato de Trabajo N° 20.744.
                </textarea>
            </p>
            <input type="submit" value="Enviar">
        </form>
    </body>
</html>

