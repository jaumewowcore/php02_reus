<?php

//Declarar variables
$contador = 10;
$numero = 2;

//Bucle for para la tabla del 2
for($i = 0; $i <= $contador; $i++){
    echo "<br>".$numero." * ".$i." = ".($numero * $i);
}
echo '<br>';

//Bucle while para la tabla del 2
$i=0;
while ($contador+1 != $i){
    echo "<br>".$numero." * ".$i." = ".($numero * $i);
    $i++;
}
echo '<br>';

//do while para la tabla del 2
$i=0;
do{
    echo "<br>".$numero." * ".$i." = ".($numero * $i);
    $i++;
}while ($contador+1 != $i);
