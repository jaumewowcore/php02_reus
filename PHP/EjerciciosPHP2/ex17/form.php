<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Ejercicio 17</title>
    </head>
    <body>
        <h1>Numero de ventas</h1>

<?php 
    if ($_SERVER['REQUEST_METHOD'] === 'POST' && 
        isset($_POST['numventas']) &&
        is_numeric($_POST['numventas'])){     
        $numVentas = $_POST['numventas'];
    ?>        
        <form action="ex17.php" method="post">
            <p>
                <?php 
                    for($i = 1; $i<=$numVentas; $i++){
                ?>
                
                <input type="number" name="venta<?=$i?>" />
                
                <?php
                    }     
                ?>
                
            </p>
            <input type="submit" value="Enviar">
        </form>
        
        
    <?php      
        }else{            
    ?>
        <form action="form.php" method="post">
            <p>
                <label for="ventas">Número de ventas</label>
                <input type="number" name="numventas" required />
            </p>
            <input type="submit" value="Enviar">
        </form>
    <?php
        }
    ?>
    </body>
</html>