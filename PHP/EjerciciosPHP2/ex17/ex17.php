<?php

$resultado = 0;

//Comprovar el metodo post por el formulario
if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    //Recorre todos los arrays de POST para obtener el valor de la venta
    for ($i = 1; $i <= sizeof($_POST); $i++) {
        $venta = "venta$i";
        
        //Comprueba si es un numero y lo suma en el resultado
        if (is_numeric($_POST[$venta]) ) {
            $resultado += $_POST[$venta];
        }
    }
    echo "<h3>El resultado es $resultado</h3>";

} else {
    retornar();
}

//En caso que no se pase el contrato vulve al formulario
function retornar() {
    echo "Valores erroneos";
    header("refresh:10;url=form.php");
}
