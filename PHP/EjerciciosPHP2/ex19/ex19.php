<?php
session_start();

if(!isset($_SESSION['intentos'])){
    $_SESSION['intentos'] = 0;
}

if(isset($_SESSION['intentos'])){    
    if(isset($_SESSION['intentos']) >= 3){
    echo "Has intentat 3 vegades de loguejarte i has fallat<br>";
    $_SESSION['intentos'] = 0;
    }
}


static $contrasena = "hola";

//Comprovar el metodo post por el formulario
if ($_SERVER['REQUEST_METHOD'] === 'POST' && !empty($_POST['contra'])) {
    echo $_SESSION['intentos'];

    $contra = $_POST['contra'];

    if (identificarse($contra) && $_SESSION['intentos'] <= 3) {
        echo "Enhorabuena";
    } else {
        echo "Error con la contraseña";
        $_SESSION['intentos']++;
        retornar();
    }
}

function identificarse($contra) {
    global $contrasena;
    
    if ($contra == $contrasena) {
        return true;
    } else {
        return false;
    }
}

//En caso que no se pase el numero vuelve al formulario
function retornar() {
    header("refresh:4;url=form.php");
}
