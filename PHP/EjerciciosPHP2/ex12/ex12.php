<?php

//Comprovar el metodo post por el formulario
if ($_SERVER['REQUEST_METHOD'] === 'POST' && !empty($_POST['numero'])) {


    $numero = $_POST['numero'];


    if (is_numeric($numero)) {
        
        if ($numero % 2 == 0) {
            echo "El numero $numero es divisible per 2";
        } else {
            echo "El numero $numero no es divisible per 2";
        }
        
    }
}

retornar();

//En caso que no se pase el numero vulve al formulario
function retornar() {
    header("refresh:4;url=form.php");
}
