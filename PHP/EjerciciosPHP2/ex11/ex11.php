<?php

//Comprovar el metodo post por el formulario
if ($_SERVER['REQUEST_METHOD'] === 'POST' && !empty($_POST['radio'])) {


    $radio = $_POST['radio'];
    
    
    if (is_numeric($radio)) {
        $resultado = pow(pi()*$radio, 2);        
        echo "El radio de $radio es $resultado";
        
    }else{
            retornar();
    }

} else {
    retornar();
}

//En caso que no se pase el radio vulve al formulario
function retornar() {
    echo "Introduce el radio";
    header("refresh:4;url=form.php");
}
