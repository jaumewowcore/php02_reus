<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Ejercicio 20</title>
    </head>
    <body>
        <h1>Calculadora</h1>

        <form action="ex20.php" method="post">
            <p>
                <label for="numero1">Numero 1</label>
                <input type="number" name="numero1" required>
            </p>

            <p>
                <label for="numero2">Numero 2</label>
                <input type="number" name="numero2" required>
            </p>
            
            <p>
                <label for="operador">Operador</label>
                <input name="operador" required>
            </p>
            <input type="submit" value="Enviar">
        </form>
    </body>
</html>