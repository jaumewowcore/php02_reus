<?php
$resultado = 0;

//Comprovar el metodo post por el formulario i si el nombre no está vacio
if ($_SERVER['REQUEST_METHOD'] === 'POST' && 
   !empty($_POST['numero1']) && !empty($_POST['numero2']) &&
   !empty($_POST['operador'])) {

    $numero1 = $_POST['numero1'];
    $numero2 = $_POST['numero2'];
    $operador = $_POST['operador'];

    //Comprueba si es mayor o menor
    if (is_numeric($numero1) && is_numeric($numero2)) {
        switch ($operador) {
            case '+':
                $resultado = ($numero1 + $numero2);
                break;
            
            case '-':
                $resultado = ($numero1 - $numero2);
                break;
            
            case '*':
                $resultado = ($numero1 * $numero2);
                break;
            
            case '/':
                $resultado = ($numero1 * $numero2);
                break;
            
            case '^':
                $resultado = pow($numero1, $numero2);
                break;
            
            case '%':
                $resultado = ($numero1 % $numero2);
                break;

            default:
                retornar();
                break;
        }
        
        echo "<h3>El resultado es $resultado</h3>";
        
    }else{
       retornar();
    }
    
} else {
    retornar();
}




//En caso que no se pase el contrato vulve al formulario
function retornar() {
    echo "Valores erroneos";
    header("refresh:10;url=form.php");
}
