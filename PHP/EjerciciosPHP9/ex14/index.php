<?php

require_once 'Persona.php';
require_once 'Empleado.php';

$empleado2 = new Persona();
$empleado2->cargar("Pau", 25);
$empleado2->mostrar();

echo "<br>";
echo "<br>";

$empleado1 = new Empleado();
$empleado1->cargar("Jaume", 22);
$empleado1->setSueldo(876);
$empleado1->mostrar();
echo $empleado1->getSueldo()."€<br><br>";
