<?php

abstract class Trabajador {
    protected $nombre;
    protected $sueldo;
    
    public abstract function calcularSueldo();
    
    public function mostrar(){
        echo "El nombre es $this->nombre i el sueldo es $this->sueldo €<br>";
    }
    
}

