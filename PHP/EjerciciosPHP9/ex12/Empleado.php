<?php

class Empleado extends Trabajador{
    protected $horasTrabajadas;
    protected $valorHora=3.50;
    
    public function calcularSueldo() {
        $this->sueldo = ($this->horasTrabajadas * $this->valorHora);
        echo $this->sueldo."<br>";
    }
    
    public function getHorasTrabajadas() {
        return $this->horasTrabajadas;
    }

    public function getValorHora() {
        return $this->valorHora;
    }

    public function setHorasTrabajadas($horasTrabajadas) {
        $this->horasTrabajadas = $horasTrabajadas;
    }

    public function setValorHora($valorHora) {
        $this->valorHora = $valorHora;
    }



}

