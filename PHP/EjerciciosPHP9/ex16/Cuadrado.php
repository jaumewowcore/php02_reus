<?php

class Cuadrado {

    protected $lado;

    public function lado($lado) {
        $this->lado = $lado;
    }

    public function calcular(Cuadrado $cuadrado) {
        return "La superfície del cuadrado es ".($cuadrado->lado * 2)."<br>"
                . "El perímetro es ".($cuadrado->lado * 4);
    }

}
