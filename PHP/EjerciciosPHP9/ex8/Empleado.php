<?php

class Empleado extends Persona{
    
    protected $sueldo;
    
    public function __construct($nombre, $edad, $sueldo) {
        parent::__construct($nombre, $edad);
        $this->sueldo = $sueldo;
    }
    
    public function getSueldo() {
        return "<br>El sueldo es ".$this->sueldo;
    }


}

