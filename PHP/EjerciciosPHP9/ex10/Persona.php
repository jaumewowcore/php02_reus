<?php

abstract class Persona{
    protected $nombre;
    protected $edad;
    
    
    public function cargar($nombre, $edad) {
        $this->nombre = $nombre;
        $this->edad = $edad;
    }

    public final function mostrar(){
        echo "El nombre $this->nombre i la edad $this->edad";
    }
    
    
    
    
    
    
}


