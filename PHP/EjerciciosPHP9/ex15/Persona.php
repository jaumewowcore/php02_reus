<?php

class Persona {

    protected $nombre;
    protected $edad;

    public function inicializar($nombre, $edad) {
        $this->nombre = $nombre;
        $this->edad = $edad;
    }

    public function __clone() {
        
    }

    public function getNombre() {
        return $this->nombre;
    }

    public function getEdad() {
        return $this->edad;
    }

}
