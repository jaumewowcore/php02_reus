<?php

require_once 'trabajador.php';
require_once 'empleado.php';
require_once 'gerente.php';

$trab = 0;
$gerentes = 0;

$trabajadores = [
    new Empleado("Marc", 900),
    new Empleado("Ricard", 500),
    new Empleado("Monica", 989),
    //Gerentes
    new Gerente("Jaume", 2000),
    new Gerente("Jeremi", 1000),
];

foreach ($trabajadores as $trabajador) {
    if($trabajador instanceof Gerente){
        $gerentes+=$trabajador->getSueldo();
    }
    
    if($trabajador instanceof Empleado){
        $trab+=$trabajador->getSueldo();
    }
    
}

echo "Los gerentes en total cobran $gerentes<br>";
echo "Los trabajadores en total cobran $trab<br>";
