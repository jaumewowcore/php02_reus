<?php

require_once 'trabajador.php';
require_once 'empleado.php';
require_once 'gerente.php';

$nombre = "";
$maxCobra = 0;

$trabajadores = [
    new Empleado("Marc", 900),
    new Empleado("Ricard", 500),
    new Empleado("Monica", 989),
    //Gerentes
    new Gerente("Jaume", 2000),
    new Gerente("Jeremi", 1000),
];

foreach ($trabajadores as $trabajador) {
    if($trabajador instanceof Gerente){
        if($trabajador->getSueldo() > $maxCobra){
            $nombre = $trabajador->getNombre();
            $maxCobra = $trabajador->getSueldo();
        }
    }
    
}

echo "<br>$nombre es el gerente que más cobra con una cantidad de $maxCobra";
