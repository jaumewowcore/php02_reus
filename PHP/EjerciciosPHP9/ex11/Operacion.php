<?php

abstract class Operacion {

    protected $valor1;
    protected $valor2;
    protected $resultado;

    public function cargar1($valor1) {
        $this->valor1 = $valor1;
    }

    public function cargar2($valor2) {
        $this->valor2 = $valor2;
    }
    
    public function getResultado() {
        return $this->resultado;
    }

    public abstract function calculo();

}
