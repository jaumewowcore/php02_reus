<?php

require_once 'Operacion.php';
require_once 'Suma.php';
require_once 'Resta.php';

$suma1 = new Suma();
$suma1->cargar1(5);
$suma1->cargar2(7);
$suma1->calculo();
echo $suma1->getResultado()."<br>";


$resta1 = new Resta();
$resta1->cargar1(5);
$resta1->cargar2(7);
$resta1->calculo();
echo $resta1->getResultado();
