<?php
$conexion = mysqli_connect("localhost", "root", "", "base1") or die("Problemas con la conexión");

$sql = "SELECT * FROM departamentos";
$departamentos = mysqli_query($conexion, $sql) or die("Problemas con el login");

//Cabecera
require_once 'cabecera.php';
?>
<h3>Añadir empleados</h3>
<form action="ex8.php" method="POST">
    <input type="hidden" name="accion" value="1" />

    <p>
        <label>DNI</label>
        <input type="text" name="dni" />
    </p>

    <p>
        <label>Nombre empleado</label>
        <input type="text" name="nombre" />
    </p>

    <p>
        <label>Apellidos empleado</label>
        <input type="text" name="apellidos" />
    </p>


    <label>Departamento</label>
    <select name="departamento">
        <?php while ($departamento = mysqli_fetch_array($departamentos)) { ?>
            <option value="<?= $departamento['codigo'] ?>"><?= $departamento['nombre'] ?></option>
            <?php
        }
        mysqli_close($conexion);
        ?>
    </select>
    <br>
    <br>
    <p>
    <h3>Añadir departamento</h3>
    <input name="ndepartamento" type="text" />
</p>

<p>
    <label>Presupuesto</label>
    <input name="presupuesto" type="number" />
</p>
<br>
<input type="submit" value="Guardar" />
</form>