<?php

$conexion = mysqli_connect("localhost", "root", "", "base1") or die("Problemas con la conexión");

if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    if (isset($_REQUEST['accion']) && !empty($_REQUEST['accion']) &&
            is_numeric($_REQUEST['accion'])) {

        switch ($_REQUEST['accion']) {
            //AÑADIR empleado
            case 1:
                if (isset($_REQUEST['nombre']) && !empty($_REQUEST['nombre']) &&
                        isset($_REQUEST['apellidos']) && !empty($_REQUEST['apellidos']) &&
                        isset($_REQUEST['dni']) && !empty($_REQUEST['dni']) &&
                        isset($_REQUEST['departamento']) && !empty($_REQUEST['departamento']) &&
                        is_numeric($_REQUEST['departamento'])) {

                    $dni = strtolower($_REQUEST['dni']);
                    $nombre = strtolower($_REQUEST['nombre']);
                    $apellidos = strtolower($_REQUEST['apellidos']);
                    $departamento = $_REQUEST['departamento'];

                    $sql = "INSERT INTO empleados(dni, nombre, apellidos, departamento) VALUES ('$dni', '$nombre', '$apellidos', $departamento)";
                    $resultado = mysqli_query($conexion, $sql) or die("Problemas con la inserción");

                    if ($resultado) {
                        echo "Empleado insertado<br>";
                        header("Refresh:4; url=index.php");
                    }
                }

                //Añadir departamento
                if (isset($_REQUEST['ndepartamento']) && !empty($_REQUEST['ndepartamento']) &&
                        isset($_REQUEST['presupuesto']) && is_numeric($_REQUEST['presupuesto'])) {

                    $departamento = strtolower($_REQUEST['ndepartamento']);
                    $presupuesto = +$_REQUEST['presupuesto'];

                    $sql = "INSERT INTO departamentos(nombre, presupuesto) VALUES ('$departamento', $presupuesto)";
                    $resultado = mysqli_query($conexion, $sql) or die("Problemas con la inserción");

                    if ($resultado) {
                        echo "Departamento insertado<br>";
                        header("Refresh:4; url=index.php");
                    }
                }

                mysqli_close($conexion);
                break;

            case 2:
                //BORRAR departamento
                if (isset($_REQUEST['departamento']) && !empty($_REQUEST['departamento']) &&
                        is_numeric($_REQUEST['departamento'])) {

                    $codigo = +$_REQUEST['departamento'];

                    $sql = "DELETE FROM departamentos WHERE codigo=$codigo";
                    $resultado = mysqli_query($conexion, $sql) or die("Problemas con el borrar" . mysqli_error($conexion));

                    if ($resultado) {
                        echo "Borrado correcto<br>";
                        header("Refresh:4; url=index.php");
                    }
                }

                //BORRAR empleado
                if (isset($_REQUEST['dni']) && !empty($_REQUEST['dni']) ) {
                    $codigo = +$_REQUEST['dni'];

                    $sql = "DELETE FROM empleados WHERE dni=$codigo";
                    $resultado = mysqli_query($conexion, $sql) or die("Problemas con el borrar" . mysqli_error($conexion));

                    if ($resultado) {
                        echo "Borrado correcto<br>";
                        header("Refresh:4; url=index.php");
                    }
                }

                mysqli_close($conexion);
                break;

            //MODIFICAR ARTICULO
            case 3:
                if (isset($_REQUEST['departamento']) && !empty($_REQUEST['departamento']) &&
                        is_numeric($_REQUEST['departamento']) && !empty($_REQUEST['nombre']) &&
                        is_numeric($_REQUEST['presupuesto']) &&
                        isset($_REQUEST['nombre']) && !empty($_REQUEST['presupuesto'])) {


                    $codigo = +$_REQUEST['departamento'];
                    $presupuesto = +$_REQUEST['presupuesto'];
                    $nombre = strtolower($_REQUEST['nombre']);

                    $sql = "UPDATE departamentos SET nombre='$nombre', presupuesto=$presupuesto WHERE codigo=$codigo";
                    $resultado = mysqli_query($conexion, $sql) or die("Problemas con el modificar" . mysqli_error($conexion));

                    if ($resultado) {
                        echo "modificar correcto";
                        header("Refresh:4; url=index.php");
                    }

                }
                
                if (isset($_REQUEST['dni']) && !empty($_REQUEST['dni']) &&
                        is_numeric($_REQUEST['emdepart']) && !empty($_REQUEST['emdepart']) &&
                        isset($_REQUEST['nombre']) && !empty($_REQUEST['nombre']) &&
                        isset($_REQUEST['apellidos']) && !empty($_REQUEST['apellidos'])) {


                    $codigo = +$_REQUEST['emdepart'];
                    $nombre = strtolower($_REQUEST['nombre']);
                    $dni = strtolower($_REQUEST['dni']);
                    $apellidos = strtolower($_REQUEST['apellidos']);

                    $sql = "UPDATE empleados SET nombre='$nombre', apellidos='$apellidos', departamento=$codigo WHERE dni='$dni'";
                    $resultado = mysqli_query($conexion, $sql) or die("Problemas con el modificar" . mysqli_error($conexion));

                    if ($resultado) {
                        echo "modificar correcto";
                        header("Refresh:4; url=index.php");
                    }

                }

                    mysqli_close($conexion);
                break;

            //BUSCAR
            case 4:
                if (isset($_REQUEST['nombre']) && !empty($_REQUEST['nombre'])) {

                    $nombre = strtolower($_REQUEST['nombre']);

                    $sql = "SELECT * FROM empleados WHERE nombre LIKE '%$nombre%' ";
                    $empleados = mysqli_query($conexion, $sql) or die("Problemas con el buscar" . mysqli_error($conexion));

                    if ($empleados) {
                        while ($empleado = mysqli_fetch_array($empleados)) {
                            echo "<br>DNI: " . $empleado['dni'];
                            echo "<br>Nombre: " . $empleado['nombre'];
                            echo "<br>Nombre: " . $empleado['apellidos'];
                            echo '<br>-----------------------------';
                        }
                    } else {
                        echo "No se han encontrado resultados";
                    }
                    
                //Buscar por apellidos
                }elseif (isset($_REQUEST['apellidos']) && !empty($_REQUEST['apellidos'])) {
                    $apellidos = strtolower($_REQUEST['apellidos']);

                    $sql = "SELECT * FROM empleados WHERE apellidos LIKE '%$apellidos%' ";
                    $empleados = mysqli_query($conexion, $sql) or die("Problemas con el buscar" . mysqli_error($conexion));

                    if ($empleados) {
                        while ($empleado = mysqli_fetch_array($empleados)) {
                            echo "<br>DNI: " . $empleado['dni'];
                            echo "<br>Nombre: " . $empleado['nombre'];
                            echo "<br>Apellidos: " . $empleado['apellidos'];
                            echo '<br>-----------------------------';
                        }
                    } else {
                        echo "No se han encontrado resultados";
                    }
                }

                mysqli_close($conexion);
                break;


            //En caso que se le pase una opcio no contemplada
            default:
                header("index.php");
                break;
        }
    }
}

if (isset($_GET['accion']) && is_numeric($_GET['accion'])) {
    $codigo = $_GET['accion'];
    //Borrar todo
    if ($codigo == 4) {

        $sql = "DELETE FROM departamentos";
        $resultado = mysqli_query($conexion, $sql) or die("Problemas con el borrar" . mysqli_error($conexion));

        if ($resultado) {
            echo "Borrar todo correcto";
            header("Refresh:4; url=index.php");
        }

        mysqli_close($conexion);
    }
}