<?php

require_once 'cabecera.php';

$conexion = mysqli_connect("localhost", "root", "", "base1") or die("Problemas con la conexión");

//Departamentos
$sql = "SELECT * FROM departamentos";
$departamentos = mysqli_query($conexion, $sql) or die("Problemas con el login");

echo "<h2>Departamentos</h2>";
while($departamento = mysqli_fetch_array($departamentos) ){
       echo "Nombre: ".$departamento['nombre'];
       echo "<br>Precio: ".$departamento['presupuesto'];
       echo '<br>-----------------------------<br>';
}

//Empleados
$sql = "SELECT e.dni, e.nombre, e.apellidos, d.nombre AS 'depart' FROM empleados e, departamentos d WHERE d.codigo=e.departamento";
$empleados = mysqli_query($conexion, $sql) or die("Problemas con el login");

echo "<h2>Empleados</h2>";
while($empleado = mysqli_fetch_array($empleados) ){
       echo "DNI: ".$empleado['dni'];
       echo "<br>Nombre: ".$empleado['nombre'];
       echo "<br>Precio: ".$empleado['apellidos'];
       echo "<br>Departamento: ".$empleado['depart'];
       echo '<br>-----------------------------<br>';
}

mysqli_close($conexion);

