/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  jaume
 * Created: 29-jul-2019
 */

CREATE DATABASE base1;
use base1;

CREATE TABLE IF NOT EXISTS cursos(
    codigo int auto_increment PRIMARY KEY,
    nombrecurso varchar(40) not null
);

CREATE TABLE IF NOT EXISTS alumnos(
    codigo int auto_increment primary key,
    nombre varchar(50) not null,
    mail varchar(70),
    codigocurso int not null,
    foreign key (codigocurso) REFERENCES cursos(codigo)
    on delete cascade 
);

CREATE TABLE IF NOT EXISTS usuario(
    id int auto_increment primary key,
    nombre varchar(100) not null unique,
    passw varchar(255) not null
);

/*
Tienda de Informatica
*/
CREATE TABLE IF NOT EXISTS fabricantes(
    codigo int auto_increment primary key,
    nombre varchar(100) not null unique
);

INSERT INTO fabricantes(nombre) VALUES ("nike");
INSERT INTO fabricantes(nombre) VALUES ("adidas");
INSERT INTO fabricantes(nombre) VALUES ("puma");

CREATE TABLE IF NOT EXISTS articulos(
    codigo int auto_increment primary key,
    nombre varchar(100) not null,
    precio int not null,
    fabricante int not null,
    foreign key (fabricante) REFERENCES fabricantes(codigo)
);


/*
LOS EMPLEADOS
*/
CREATE TABLE IF NOT EXISTS departamentos(
    codigo int auto_increment primary key,
    nombre varchar(100) not null,
    presupuesto int UNSIGNED
);

INSERT INTO departamentos(nombre, presupuesto) VALUES ("contabilidad", 300);
INSERT INTO departamentos(nombre, presupuesto) VALUES ("ventas", 500);

CREATE TABLE IF NOT EXISTS empleados(
    dni varchar(8) primary key,
    nombre varchar(100) not null,
    apellidos varchar(255) not null,
    departamento int not null,
    foreign key (departamento) REFERENCES departamentos(codigo)
    on delete cascade
);



/*
LOS ALMACENES
*/
CREATE TABLE IF NOT EXISTS almacenes(
    codigo int auto_increment primary key,
    lugar varchar(100) not null,
    capacidad int not null
);

INSERT INTO almacenes(lugar, capacidad) VALUES ("REUS", 400);
INSERT INTO almacenes(lugar, capacidad) VALUES ("Barcelona", 400);
INSERT INTO almacenes(lugar, capacidad) VALUES ("Tarragona", 200);
INSERT INTO almacenes(lugar, capacidad) VALUES ("Reus", 300);


CREATE TABLE IF NOT EXISTS cajas(
    numReferencia char(5) primary key,
    contenido varchar(100) not null,
    valor int not null,
    almacen int not null,
    foreign key (almacen) REFERENCES almacenes(codigo)
);

INSERT INTO cajas(numReferencia, contenido, valor, almacen) VALUES ("A000", "FRESAS", 20, 1);
INSERT INTO cajas(numReferencia, contenido, valor, almacen) VALUES ("A001", "MELOCOTON", 20, 2);
INSERT INTO cajas(numReferencia, contenido, valor, almacen) VALUES ("A002", "SANDIA", 20, 3);
INSERT INTO cajas(numReferencia, contenido, valor, almacen) VALUES ("A003", "BROCOLI", 20, 1);




/*
PELICULAS Y SALAS
*/
CREATE TABLE IF NOT EXISTS peliculas(
    codigo int auto_increment primary key,
    nombre varchar(100) not null,
    calificacionEdad int not null
);

INSERT INTO peliculas(nombre, calificacionEdad) VALUES ("Narnia", 3);
INSERT INTO peliculas(nombre, calificacionEdad) VALUES ("Vikingos", 7);
INSERT INTO peliculas(nombre, calificacionEdad) VALUES ("El zorro", 18);


CREATE TABLE IF NOT EXISTS salas(
    codigo int auto_increment primary key,
    nombre varchar(100) not null,
    pelicula int not null,
    foreign key (pelicula) REFERENCES peliculas(codigo)
);