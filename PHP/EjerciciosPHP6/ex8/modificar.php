<?php

$conexion = mysqli_connect("localhost", "root", "", "base1") or die("Problemas con la conexión");

$sql = "SELECT * FROM articulos";
$articulos = mysqli_query($conexion, $sql) or die("Problemas con el login");

//Cabecera
require_once 'cabecera.php';
?>
<h1>Modificar Articulos</h1>
<form action="ex8.php" method="POST">
    <input type="hidden" name="accion" value="3" />
        
    <label>Articulo a modificar</label>
    <select name="articulo">
    <?php   
    while($articulo = mysqli_fetch_array($articulos) ){?>
            <option value="<?=$articulo['codigo']?>"><?=$articulo['nombre']?></option>
    <?php   
    }
    mysqli_close($conexion);
    ?>
    </select>
    
    <p>
        <label>Nombre</label>
        <input type="text" name="nombre" />
    </p>
    
    <p>
        <label>Precio</label>
        <input type="number" name="precio" />
    </p>
    <br>
    <input type="submit" value="Modifar articulo" />
</form>