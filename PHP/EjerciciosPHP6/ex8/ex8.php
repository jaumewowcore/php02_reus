<?php

$conexion = mysqli_connect("localhost", "root", "", "base1") or die("Problemas con la conexión");

if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    if (isset($_REQUEST['accion']) && !empty($_REQUEST['accion']) &&
            is_numeric($_REQUEST['accion'])) {

        switch ($_REQUEST['accion']) {
            //AÑADIR ARTICULO
            case 1:
                if (isset($_REQUEST['articulo']) && !empty($_REQUEST['articulo']) &&
                        isset($_REQUEST['precio']) && !empty($_REQUEST['precio']) &&
                        is_numeric($_REQUEST['precio']) &&
                        is_numeric($_REQUEST['fabricante']) && !empty($_REQUEST['fabricante'])) {

                    $nombre = strtolower($_REQUEST['articulo']);
                    $precio = $_REQUEST['precio'];
                    $fabricante = $_REQUEST['fabricante'];

                    $sql = "INSERT INTO articulos(nombre, precio, fabricante) VALUES ('$nombre', $precio, $fabricante)";
                    $resultado = mysqli_query($conexion, $sql) or die("Problemas con la inserción");

                    if ($resultado) {
                        echo "Articulo insertado<br>";
                        header("Refresh:4; url=index.php");
                    }

                }
                
                //Añadir fabricante
                if (isset($_REQUEST['nfabricante']) && !empty($_REQUEST['nfabricante']) ) {
                    $nombrefabr = strtolower($_REQUEST['nfabricante']);
                   
                    $sql = "INSERT INTO fabricantes(nombre) VALUES ('$nombrefabr')";
                    $resultado = mysqli_query($conexion, $sql) or die("Problemas con la inserción");

                    if ($resultado) {
                        echo "Fabricante insertado<br>";
                        header("Refresh:4; url=index.php");
                    }

                }

                mysqli_close($conexion);
                break;

            //BORRAR ARTICULO
            case 2:
                if (isset($_REQUEST['articulo']) && !empty($_REQUEST['articulo']) &&
                        is_numeric($_REQUEST['articulo'])) {

                    $codigo = +$_REQUEST['articulo'];

                    $sql = "DELETE FROM articulos WHERE codigo=$codigo";
                    $resultado = mysqli_query($conexion, $sql) or die("Problemas con el borrar" . mysqli_error($conexion));

                    if ($resultado) {
                        echo "Borrado correcto";
                        header("Refresh:4; url=index.php");
                    }

                    mysqli_close($conexion);
                }

                break;

            //MODIFICAR ARTICULO
            case 3:
                if (isset($_REQUEST['articulo']) && !empty($_REQUEST['articulo']) &&
                        is_numeric($_REQUEST['articulo']) && !empty($_REQUEST['precio']) &&
                        is_numeric($_REQUEST['precio']) &&
                        isset($_REQUEST['nombre']) && !empty($_REQUEST['nombre'])) {


                    $codigo = +$_REQUEST['articulo'];
                    $precio = +$_REQUEST['precio'];
                    $nombre = strtolower($_REQUEST['nombre']);

                    $sql = "UPDATE articulos SET nombre='$nombre', precio=$precio WHERE codigo=$codigo";
                    $resultado = mysqli_query($conexion, $sql) or die("Problemas con el modificar" . mysqli_error($conexion));

                    if ($resultado) {
                        echo "modificar correcto";
                        header("Refresh:4; url=index.php");
                    }

                    mysqli_close($conexion);
                }

                break;

            //BUSCAR
            case 4:
                if (isset($_REQUEST['nombre']) && !empty($_REQUEST['nombre'])) {

                    $nombre = strtolower($_REQUEST['nombre']);

                    $sql = "SELECT * FROM articulos WHERE nombre LIKE '%$nombre%' ";
                    $articulos = mysqli_query($conexion, $sql) or die("Problemas con el buscar" . mysqli_error($conexion));

                    if ($articulos) {
                        while($articulo = mysqli_fetch_array($articulos) ){
                        echo "<br>Nombre: ".$articulo['nombre'];
                        echo "<br>Precio: ".$articulo['precio'];
                        echo '<br>-----------------------------';
}
                        
                    } else {
                        echo "No se han encontrado resultados";
                    }

                    mysqli_close($conexion);
                }

                break;


            //En caso que se le pase una opcio no contemplada
            default:
                header("index.php");
                break;
        }
    }
}

if (isset($_GET['accion']) && is_numeric($_GET['accion'])) {
    $codigo = $_GET['accion'];
    //Borrar todo
    if ($codigo == 4) {

        $sql = "DELETE FROM articulos";
        $resultado = mysqli_query($conexion, $sql) or die("Problemas con el borrar todo los articulos" . mysqli_error($conexion));

        $sql = "DELETE FROM fabricantes";
        $resultado = mysqli_query($conexion, $sql) or die("Problemas con el borrar todos los fabricantes" . mysqli_error($conexion));

        if ($resultado) {
            echo "Borrar todo correcto";
            header("Refresh:4; url=index.php");
        }

        mysqli_close($conexion);
    }
}