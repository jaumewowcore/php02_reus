<?php

require_once 'Electrodomestico.php';
require_once 'Lavadora.php';
require_once 'Television.php';

$totalprice = 0;
$totalLavadora = 0;
$totalTv = 0;

$electrodomesticos = [
    //Lavadoras
    new Lavadora(150, 10, "verde", "c", 40),
    new Lavadora(100, 40, "amarillo", "a", 20),
    new Lavadora(150, 50, "rojo", "b", 30),
    new Lavadora(300, 30, "azul", "d", 10),
    new Lavadora(400, 24, "rojo", "f", 65),
    
    //TV
    new Television(150, 10, "verde", "c", 41, false), 
    new Television(100, 30, "negro", "c", 24, true), 
    new Television(170, 20, "negro", "c", 24, true), 
    new Television(176, 25, "azul", "f", 30, false), 
    new Television(100, 45, "rojo", "c"), 
];


foreach ($electrodomesticos as $electrodomestico) {
    echo get_class($electrodomestico)."<br>";
    
    if($electrodomestico instanceof Lavadora){        
        $electrodomestico->precioFinal();
        $totalLavadora+= $electrodomestico->getPrecio();        
    }
    
    if($electrodomestico instanceof Television){        
        $electrodomestico->precioFinal();
        $totalTv+= $electrodomestico->getPrecio();        
    }
     echo "<br>";     
     echo "<br>";     
}

echo "<br>La suma de las lavadoras es $totalLavadora";
echo "<br>La suma de los televisores es $totalTv";
echo "<br>La suma <strong>total</strong> de los articulos es ".($totalLavadora + $totalTv);

