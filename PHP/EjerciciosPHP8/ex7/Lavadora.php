<?php

class Lavadora extends Electrodomestico {

    private $carga = 5;

    public function __construct($precio, $peso, $color, $consumo, $carga) {
        parent::__construct($precio, $peso);       
        $this->peso = $peso;
        $this->comprobarColor($color);
        $this->comprobarConsumoEnergetico($consumo);
        $this->carga = $carga;
    }
    
    public function getCarga() {
        return $this->carga;
    }
    
    public function precioFinal() {
        parent::precioFinal();
        
        if($this->getCarga() > 30){
            $this->precio +=50;
        }
        
        echo "El precio final es ".$this->getPrecio();
    }



}
