<?php

class Electrodomestico {

    protected $precio = 100;
    protected $color = "blanco";
    protected $consumo = "F";
    protected $peso = 5;

//    const precio = 100;
//    const color = "blanco";
//    const consumo = "F";
//    const peso = 5;

    public function __construct($precio, $peso) {
        $this->precio = $precio;
        $this->peso = $peso;
    }

    public function comprobarConsumoEnergetico($letra) {
        if (is_string($letra)) {
            $letra = strtoupper($letra);
            
            if (preg_match("/[A-F]/", $letra) == 1) {
                $this->consumo = $letra;
            }
        }
    }

    public function comprobarColor($color) {
        if (is_string($color)) {
            $color = strtolower($color);
           
            switch ($color) {
                case "negro":
                    $this->color = "negro";
                    break;
                
                case "rojo":
                    $this->color = "rojo";
                    break;
                
                case "azul":
                    $this->color = "azul";
                    break;
                
                case "gris":
                    $this->color = "gris";
                    break;
                
            }
        }
    }
    
    public function precioFinal(){
        //Consumo
        switch ($this->consumo) {
            case "A": $this->precio +=100;
                break;
            case "B": $this->precio +=80;
                break;
            case "C": $this->precio +=60;
                break;
            case "D": $this->precio +=50;
                break;
            case "E": $this->precio +=30;
                break;
            case "F": $this->precio +=10;
                break;
        }
        
        //Tamaño
        if($this->peso>=0 && $this->peso<=19)
            $this->precio +=10;
        
        else if($this->peso>=20 && $this->peso<=49)
            $this->precio +=50;
        
        else if($this->peso>=50 && $this->peso<=79)
            $this->precio +=80;
        
        else if($this->peso>=80)
            $this->precio +=100;

    }
    
    
    public function getPrecio() {
        return $this->precio;
    }

    public function getColor() {
        return $this->color;
    }

    public function getConsumo() {
        return $this->consumo;
    }

    public function getPeso() {
        return $this->peso;
    }

}
