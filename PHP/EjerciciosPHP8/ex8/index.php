<?php

require_once 'Persona.php';
require_once 'Estudiante.php';
require_once 'Profesor.php';
require_once 'Aula.php';


$estudiantes = [
    new Estudiante("Pere", 13, "H", 8),
    new Estudiante("Carles", 13, "H", 7),
    new Estudiante("Arnau", 13, "H", 7),
    new Estudiante("Josep", 13, "H", 7),
    new Estudiante("Jose", 14, "H", 6),
    new Estudiante("Luisao", 17, "H", 4),
    new Estudiante("Jordi", 17, "H", 3),
];

$aula1 = new Aula(1, "matematicas", 6, 20);
$ernesto = new Profesor("ernesto", 45, "H", "matematicas");

$aula1->assignarProfe($ernesto);
echo "<br>";
echo "<br>";
foreach ($estudiantes as $estudiante) {
    $aula1->assignarAlumnos($estudiante);
}

echo "<br>";
echo "<br>";

$aula1->darClase($ernesto, $estudiantes);