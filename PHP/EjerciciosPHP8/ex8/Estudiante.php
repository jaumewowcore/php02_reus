<?php

 class Estudiante extends Persona{
     
     protected $calificacion;
     
    public function __construct($nombre, $edad, $sexo, $calificacion) {
        Persona::__construct($nombre, $edad, $sexo="indefinido");
        $this->calificacion = $calificacion;
    }
    
    public function getCalificacion() {
        return $this->calificacion;
    }

    public function setCalificacion($calificacion) {
        $this->calificacion = $calificacion;
    }




     
 }
