<?php

 class Empleado extends Persona{
     
     protected $sueldo;
     
    public function __construct($nombre, $edad, $sueldo) {
        Persona::__construct($nombre, $edad);
        $this->sueldo = $sueldo;
    }
    
    public function getSueldo() {
        return $this->sueldo;
    }

    public function setSueldo($sueldo) {
        $this->sueldo = $sueldo;
    }

    public function imprimir(){
        parent::imprimir();
        echo "Tine un sueldo de $this->sueldo";
    }
     
 }
