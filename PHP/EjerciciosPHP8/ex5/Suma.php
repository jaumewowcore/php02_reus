<?php

 class Suma extends Operacion{
     
     private $texto;

     public function __construct($v1, $v2, $texto) {
        Operacion::__construct($v1, $v2);
        $this->texto = $texto;
    }
    
    public function operar(){
        $this->resultado = ($this->valor1 + $this->valor2);
        echo $this->texto."<br>";
        $this->imprimirResultado();
    }
     
 }
