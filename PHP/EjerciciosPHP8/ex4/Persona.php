<?php

class Persona{
    protected $nombre;
    protected $edad;
    
    function __construct($nombre, $edad) {
        $this->nombre = $nombre;
        $this->edad = $edad;
    }

    public function cargarDatos($nombre, $edad){
        $this->nombre = $nombre;
        $this->edad = $edad;
    }
    
   
    
    public function imprimir(){
        echo "El es $this->nombre i tiene $this->edad años<br>";
    }
    
}
