<?php

 class Suma extends Operacion{
     
    public function __construct($v1, $v2) {
        Operacion::__construct(10, $v2);
    }
    
    public function operar(){
        $this->resultado = ($this->valor1 + $this->valor2);
        $this->imprimirResultado();
    }
     
 }
