<?php

class Operacion{
    protected $valor1;
    protected $valor2;
    protected $resultado;
    
    function __construct($valor1, $valor2) {
        $this->valor1 = $valor1;
        $this->valor2 = $valor2;
    }

    public function cargar1($valor){
        $this->valor1 = $valor;
    }
    
    public function cargar2($valor){
        $this->valor2 = $valor;
    }
    
    public function imprimirResultado(){
        echo "El resultado es $this->resultado<br>";
    }
    
}
