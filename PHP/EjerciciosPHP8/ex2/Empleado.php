<?php

 class Empleado extends Persona{
     
     protected $sueldo;
     
    public function __construct($nombre, $edad, $sueldo) {
        Persona::__construct($nombre, $edad);
        $this->sueldo = $sueldo;
    }
    
    function getSueldo() {
        return $this->sueldo;
    }

    function setSueldo($sueldo) {
        $this->sueldo = $sueldo;
    }


     
 }
