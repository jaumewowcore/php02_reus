addEventListener('load', inicialitzarEventos, false);

function inicialitzarEventos() {

    var ob = document.getElementById('boton1');
    ob.addEventListener('click', presionBoton, false);

}

var conexion1;
function presionBoton() {
    var codigo = document.getElementById('codigo').value;
    conexion1 = new XMLHttpRequest();
    conexion1.onreadystatechange = procesarEventos;
    conexion1.open("GET", "pagina1.php?codigo="+codigo, true);
    conexion1.send();
}

function procesarEventos() {
    var resultados = document.getElementById("resultados");
    if (conexion1.readyState == 4) {
        var datos = JSON.parse(conexion1.responseText);
            var imprimir = "";
            
            for(var i = 0; i < datos.length; i++){
                imprimir += "Codigo: "+datos[i].codigo+"<br>";
                imprimir += "Descripción: "+datos[i].descripcion+"<br>";
                imprimir += "Precio: "+datos[i].precio+"<br><br>";
            }
            
            resultados.innerHTML = imprimir;                 

    } else {
        resultados.innerHTML = "Cargando.."
    }
}
