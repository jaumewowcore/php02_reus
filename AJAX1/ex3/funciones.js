addEventListener('load',inicialitzarEventos, false);

function inicialitzarEventos(){
    
    var ob = document.getElementById('boton1');
    ob.addEventListener('click', presionBoton, false);
    
}

function presionBoton(e){
    var dni = document.getElementById('dni').value;
    recuperarDatos(dni);    
} 


var conexion1;
function recuperarDatos(dni){
    conexion1 = new XMLHttpRequest();
    conexion1.onreadystatechange = procesarEventos;
    conexion1.open("GET", "pagina1.php?dni="+dni, true);
    conexion1.send();
}

function procesarEventos(){
    var resultados = document.getElementById("resultados");
    if(conexion1.readyState == 4){
        var datos =  JSON.parse(conexion1.responseText);
        var imprimir = "Nombre: "+datos.nombre+"<br>";
        imprimir += "Apellido: "+datos.apellido+"<br>";
        imprimir += "Dirección donde tiene que votar: "+datos.direccion;
        resultados.innerHTML = imprimir;
        
    }else{
        resultados.innerHTML = "Cargando.."
    }
}
