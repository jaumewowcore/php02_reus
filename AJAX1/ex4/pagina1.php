<?php

header('Content-Type: text/html; charset=utf-8');

class Persona {
    
}

$persona = new Persona();

if ($_REQUEST['dni'] == 1) {
    $persona->nombre = "Jaume";
    $persona->apellido = "Balaña";
    $persona->direccion = "Calle de jaume";

    //Convertir objeto
    $json = json_encode($persona);
    echo $json;
    
} else if ($_REQUEST['dni'] == 2) {
    $persona->nombre = "Ana";
    $persona->apellido = "Castanyes";
    $persona->direccion = "Calle de castanya";
    
    //Convertir objeto
    $json = json_encode($persona);
    echo $json;
    
} else if ($_REQUEST['dni'] == 3) {
    $persona->nombre = "Pere";
    $persona->apellido = "Emorzador";
    $persona->direccion = "Plaza de sant miquel";
    
    //Convertir objeto
    $json = json_encode($persona);
    echo $json;
    
} else {
    echo json_encode((object) null);
}

     
