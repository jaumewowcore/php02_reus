CREATE DATABASE bdajax;
USE bdajax;

CREATE TABLE perifericos(
    codigo INT not null AUTO_INCREMENT PRIMARY KEY,
    descripcion TEXT not null,
    precio DOUBLE(6,2) not null
);