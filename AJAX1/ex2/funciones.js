addEventListener('load',inicialitzarEventos, false);

function inicialitzarEventos(){    
    var ob = document.getElementById('boton1');
    ob.addEventListener('click', botonPresionado, false);
    
}

function botonPresionado(e){
    var dni = document.getElementById('dni').value;
    var nombre = document.getElementById('nombre').value;
    var fecha = document.getElementById('fecha').value;
    var sueldo1 = document.getElementById('sueldo1').value;
    var sueldo2 = document.getElementById('sueldo2').value;
    var sueldo3 = document.getElementById('sueldo3').value;

    var obj = {
        nombre: nombre,
        dni: dni,
        fecha: fecha,
        sueldos:[sueldo1, sueldo2, sueldo3]
    };
    var cadena = JSON.stringify(obj);
    enviarDatos(cadena);    
} 


var conexion1;
function enviarDatos(cadena){
    conexion1 = new XMLHttpRequest();
    conexion1.onreadystatechange = procesarEventos;
    conexion1.open("GET", "pagina1.php?cadena="+cadena, true);
    conexion1.send(null);
}

function procesarEventos(){
    var resultados = document.getElementById("resultados");
    if(conexion1.readyState == 4){
        resultados.innerHTML = conexion1.responseText;
        
    }else{
        resultados.innerHTML = "Cargando.."
    }
}
