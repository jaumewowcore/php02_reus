addEventListener('load',inicialitzarEventos, false);

function inicialitzarEventos(){
    
    var ob = document.getElementById('boton1');
    ob.addEventListener('click', presionBoton, false);
    
}

function presionBoton(e){
    //Creación de JSON
    var cadena = '{"persona":"jaume",' +
                 '"cotxe":"si",' +
                 '"lenguajes":["PHP","JS"]' +
                 '}';
    
    var persona = JSON.parse(cadena);
    
    //Imprimir valores
    var imprimir = "Nombre: "+persona.persona;
    imprimir += " Permiso de conducir: "+persona.cotxe;
    imprimir += " Lenguajes: "+persona.lenguajes[0]+ " "+persona.lenguajes[1];
    
    alert(imprimir);
    
    
} 