<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ArticulosController extends Controller
{
    public function catalog(){
        return view('catalog.index');
    }
    
    public function show($id){
        return view('catalog.show')->with('identificador', $id);
    }
    
    public function create(){
        return view('catalog.create');
    }
    
    public function edit($id){
        return view('catalog.edit', ['identificador' => $id]);
    }
}
