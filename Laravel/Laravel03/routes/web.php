<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('login', function () {
    return view('auth.login');
});

Route::get('logout', function () {
    return "logout";
});

Route::get('catalog', 'ArticulosController@catalog');
Route::get('catalog/show/{id}', 'ArticulosController@show');
Route::get('catalog/create', 'ArticulosController@create');
Route::get('catalog/edit/{id}', 'ArticulosController@edit');
