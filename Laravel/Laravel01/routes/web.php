<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/test', function () {
    echo "Esto es una simple prueba";
});


//Another routes
Route::get('/probando/ruta', function () {
    return 'get';
});

Route::post('/probando/ruta', function () {
    return 'post';
});

Route::put('/probando/ruta', function () {
    return 'put';
});

Route::delete('/probando/ruta', function () {
    return 'delete';
});




Route::match(['get', 'post', 'put'], '/testing', function () {
    echo "Ruta testing para los verbos GET, POST, PUT";
});

Route::any('/cualquiercosa', function () {
    //Se ejecuta el codigo en cualquier accion
    return 'La ruta /cualquiercosa asociada a cualquier verbo';
});


//Rutas con parametros
Route::get('/colaboradores/{nombre}', function ($nombre) {
        echo "Buscando el colaborador $nombre";
});

//Route::get('/tienda/productos/{id}', function ($id_producto) {
//        echo "Buscando el producto $id_producto de la tienda";
//});

Route::get('/agenda/{mes}/{ano}', function ($mes, $ano) {
    return "Viendo la agenda de $mes de $ano";
});

Route::get('tienda/productos/{id}', 'TiendaController@producto');

Route::get('/categoria/{categoria}', function ($categoria) {
    return "Ruta 1- Viendo categoría $categoria y no recibe página";
});

Route::get('/categoria/{categoria}/{pagina?}', function ($categoria, $pagina=1) {
    return "Ruta 1- Viendo categoría $categoria y página $pagina";
});

Route::get('/colaboradores/{nombre}', function ($nombre) {
    return "Mostrando el colaborador $nombre";
})->where(array("nombre" => "[a-zA-Z]+"));