<form action="deposito" method="POST">
    @csrf

    <p>
        <label for="caudal">Caudal litros por minuto</label>
        <input type="text" name="caudal" value="{{old('caudal')}}" />
        @if ($errors->has('caudal'))
        <span>{{ $errors->first('caudal') }}</span>
        @endif
    </p>

    <p>
        <label for="volumen">Volumen deposito</label>
        <input type="text" name="volumen" value="{{old('volumen')}}" />
        @if ($errors->has('volumen'))
        <span>{{ $errors->first('volumen') }}</span>
        @endif
    </p>


    <input type="submit" value="Calcular">
</form>