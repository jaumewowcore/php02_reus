<h1 align="center">Pagina de Login</h1>

@if(count($errors) > 0)
    <h3>Login incorrecto</h3>
@endif

<form action="comprovar" method="POST">
    @csrf
    <div class="container">

        <p>
            <label for="name"><b>Nombre</b></label>
            <input type="text" name="name" required>
        </p>

        <p>
            <label for="password"><b>Password</b></label>
            <input type="password" name="password" required>
        </p>

        <br>

        <input type="submit" value="Comprovar" />
    </div>


</form>