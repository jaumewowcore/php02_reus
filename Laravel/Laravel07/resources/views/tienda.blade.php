<form action="tienda" method="POST">
    @csrf
    <p>
        <label for="tienda1">Tienda 1 precio</label>
        <input type="number" name="tienda1" value="{{old('tienda1')}}" />
        @if ($errors->has('tienda1'))
        <span>{{ $errors->first('tienda1') }}</span>
        @endif
    </p>
        
    <p>
        <label for="tienda2">Tienda 2 precio</label>
        <input type="number" name="tienda2" value="{{old('tienda2')}}" />
        @if ($errors->has('tienda2'))
        <span>{{ $errors->first('tienda2') }}</span>
        @endif
    </p>
        
    <p>
        <label for="tienda3">Tienda 3 precio</label>
        <input type="number" name="tienda3" value="{{old('tienda3')}}" />
        @if ($errors->has('tienda3'))
        <span>{{ $errors->first('tienda3') }}</span>
        @endif
    </p>
        

    <input type="submit" value="Calcular">
</form>