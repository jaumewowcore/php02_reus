<form action="cilindro" method="POST">
    @csrf
    <p>
        <label for="altura">Altura</label>
        <input type="number" name="altura" value="{{old('altura')}}" />
        @if ($errors->has('altura'))
        <span>{{ $errors->first('altura') }}</span>
        @endif

    </p>

    <p>
        <label for="diametro">Diametro</label>
        <input type="text" name="diametro" value="{{old('diametro')}}" />
        @if ($errors->has('diametro'))
        <span>{{ $errors->first('diametro') }}</span>
        @endif
    </p>

    <input type="submit" value="Calcular">
</form>