<meta charset="UTF-8">
<h1 align="center">Pais</h1>

@php
$pais = array();
$pais["Mexico"] = array("Guadalajara", "Monterrey", "Tepic", "Pachuca", "D.F.");
$pais["España"] = array("Barcelona", "Madrid", "Valencia", "Mallorca", "Osasuna");
$pais["EUA"] = array("Houston", "Washington", "Seattle", "Manhattan", "San Francisco");
$pais["Francia"] = array("París", "Tolousse", "St. Ettienne", "Marsella", "Nancy");
$pais["Alemania"] = array("Münich", "Colonia", "Monchengladbach", "Dormund", "Leverkusen");
$pais["Canada"] = array("Montreal", "Ottawa", "Vancouver", "Edmonton", "Quebec");
$pais["Inglaterra"] = array("Londres", "Manchester", "West Ham", "Liechtester", "Chelsea");
$pais["Brasil"] = array("Sao Paolo", "Brasilia", "Rio de Janeiro", "Porto Alegre", "Cotia");
$pais["Italia"] = array("Roma", "Milán", "Venecia", "Cagliari", "Palermo");
$pais["Japón"] = array("Tokio", "Okynawa", "Hiroshima", "Nagasaki", "Kioto");
@endphp


<form action="pais" method="POST">
    @csrf
    <p>
        <label for="country">Pais</label>
        <select name="country">
            @foreach($pais as $key => $value)
                @if (isset($country) && $country != $key) 
                    <option value='{{$key}}'>{{$key}}</option>";
                @else
                    <option value='{{$key}}' selected>{{$key}}</option>";
                @endif
            @endforeach
            
        </select>
    </p>

    <input type="submit" value="Buscar">
</form>


@if (isset($country) && !empty($country) ) 
    @foreach ($pais[$country] as $value) 
        <br>{{$value}}
    @endforeach
@endif


</body>
</html>
