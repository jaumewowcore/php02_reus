<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DepositoController extends Controller {

    public function listar() {
        return view('deposito');
    }

    public function guardar(Request $request) {
        $this->validate($request, [
            'caudal' => 'required|numeric',
            'volumen' => 'required|numeric'
        ]);

        $resultado = $request->volumen / $request->caudal;

        return "Tardara $resultado minutos";
    }

}
