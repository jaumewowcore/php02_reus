<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LoginController extends Controller {

    public function login() {
        $pais = \Request::cookie('pais', '16pt');

        return view('tabla', [
            'pais' => $pais
        ]);
    }

    public function comprovar(Request $request) {
        $errors = ['errors' => 'Errores en el login'];

        $this->validate($request, [
            'name' => 'required',
            'password' => 'required'
        ]);

        if ($request->input('name') == 'Gomez' &&
                $request->input('password') == 'alibaba') {
            return "Login correcto";
        }

        return redirect()->route('profile')
           ->with('errors', $errors);
    }

}
