<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TiendaController extends Controller {

    public function listar() {
        return view('tienda');
    }

    public function guardar(Request $request) {
        $this->validate($request, [
            'tienda1' => 'required|numeric',
            'tienda2' => 'required|numeric',
            'tienda3' => 'required|numeric'
        ]);

        $resultado = ($request->tienda1 + $request->tienda2 + $request->tienda3 ) / 3;

        return "El precio medio es de $resultado €";
    }

}
