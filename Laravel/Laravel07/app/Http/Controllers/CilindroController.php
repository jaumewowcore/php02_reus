<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CilindroController extends Controller {

    public function listar() {
        return view('cilindro');
    }

    public function guardar(Request $request) {
        $this->validate($request, [
            'altura' => 'required|numeric',
            'diametro' => 'required|numeric'
        ]);

        $resultado = pi() * pow($request->diametro, 2) * $request->altura;

        return "El volumen es $resultado";
    }

}
