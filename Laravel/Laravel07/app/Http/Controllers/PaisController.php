<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PaisController extends Controller
{
   public function listar(){
       $pais = \Request::cookie('country', 'España');
       
       return view('tabla', [
          'country' =>  $pais
       ]);
   }
   
   public function guardarPais(Request $request){
       $this->validate($request, ['country' => 'required']);
       
       return redirect('pais')
            ->withCookie(cookie('country', $request->input('country'), 60 * 24 * 365));
   }
}
