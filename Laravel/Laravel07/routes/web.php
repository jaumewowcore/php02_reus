<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login', 'LoginController@login');
Route::post('comprovar', 'LoginController@verif');

Route::get('pais', 'PaisController@listar');
Route::post('pais', 'PaisController@guardarPais');

Route::get('cilindro', 'CilindroController@listar');
Route::post('cilindro', 'CilindroController@guardar');

Route::get('tienda', 'TiendaController@listar');
Route::post('tienda', 'TiendaController@guardar');

Route::get('deposito', 'DepositoController@listar');
Route::post('deposito', 'DepositoController@guardar');