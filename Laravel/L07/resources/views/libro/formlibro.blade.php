<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Crear un libro</title>
    </head>
    <body>
        <h1>Crear libro</h1>
        @if(count($errors) > 0)
        <div class="errors">
            <ul>
                @foreach($errors->all() as $error)
                <li>
                    {{ $error }}
                </li>
                @endforeach
            </ul>
        </div>
        @endif
        
        <form action="crear" method="post">
            @csrf
            Nombre: <input type="text" name="name" value="{{old('name')}}" />
            <br>
            Author: <input type="text" name="author" value="{{old('author')}}" />
            <br>
            ISBN: <input type="text" name="isbn" value="{{old('isbn')}}" />
            <br>
            <input type="submit" value="Crear">
        </form>


    </body>
</html>

