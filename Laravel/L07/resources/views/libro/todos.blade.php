<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Todos los libros</title>
    </head>
    <body>
        @foreach($libros as $libro)
        <p>
            {{$libro['name']}}, por {{$libro['author']}}
            <br>
            ISBN: {{$libro['isbn']}}
        </p>
        @endforeach
    </body>
</html>

