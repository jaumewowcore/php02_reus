<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Book;

class BookSeeder extends Seeder
{
   
    public function run()
    {
        Book::create([
            'name' => 'Viaje al centro de la tierra',
            'author' => 'Julio Verne',
            'isbn' => '14444585'
        ]);
    }
}
