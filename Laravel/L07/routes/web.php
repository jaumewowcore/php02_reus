<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('libros', 'BookController@index');
Route::get('libros/{id}', 'BookController@show')
->where(['id' => '[0-9]+']);
Route::get('libros/crear', 'BookController@create');
Route::post('libros/crear', 'BookController@store');

Route::get('rawsql/libros/buscarid', function(){
    $id = \Request::input('id');
    $libros = DB::select('select * from books where id='.$id);
    dd($libros);
})->where(['id' => '[0-9]+']);

Route::get('rawsql/libros/insertar', function(){
    $titulo = "El quijote";
    $autor = "Cervantes";
    $insertado = DB::insert('insert into books (name, author) values (?, ?)', [$titulo, $autor]);
});

