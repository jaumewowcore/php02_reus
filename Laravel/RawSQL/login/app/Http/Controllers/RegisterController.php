<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class RegisterController extends Controller {

    public function vista() {
        return view('register');
    }

    public function registrarse(Request $request) {
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:4',
        ]);

        $password = md5($request->password);
        
        DB::insert('insert into users (name, email, password) values (?, ?, ?)',
                [$request->name, $request->email, $password]);
        
        return redirect()->action('LoginController@vista');

    }

}
