<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class LoginController extends Controller {

    public function vista() {
        return view('login');
    }

    public function autentificarse(Request $request) {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:4',
        ]);

        $password = md5($request->password);

        $user = DB::table('users')->where('email', $request->email)->first();

        if (!is_object($user)) {
            return "Login incorrecto";
        }

        if ($password == $user->password) {
            return "Login correcto";
        } else {
            return "Login incorrecto";
        }
    }

}
