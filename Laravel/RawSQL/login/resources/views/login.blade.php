<h1>Login</h1>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<br>
<br>
<form  method="POST" action="login">
    @csrf
    <div class="container">
        <p>
            <label for="email"><b>Correo electronico</b></label>
            <input type="email" name="email" required value="{{ old('email') }}" />

        </p>
        <p>
            <label for="password"><b>Contraseña</b></label>
            <input type="password" name="password" required value="{{ old('password') }}" />
        </p>

        <p>
            <button type="submit">Login</button>
        </p>

    </div>

    <div class="container" style="background-color:#f1f1f1">
        <span class="psw">Forgot <a href="#">password?</a></span>
    </div>
</form>