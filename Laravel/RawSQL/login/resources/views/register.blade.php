<h1>Registro</h1>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<br>
<br>
<form  method="POST" action="registrar">
    @csrf
    <div class="container">
        <p>
            <label for="name"><b>Nombre</b></label>
            <input type="text" name="name" required value="{{ old('name') }}" />
        </p>
        
        <p>
            <label for="email"><b>Correo electronico</b></label>
            <input type="email" name="email" required value="{{ old('email') }}" />
        </p>
        
        <p>
            <label for="password"><b>Contraseña</b></label>
            <input type="password" name="password" required value="{{ old('password') }}" />
        </p>

        <p>
            <button type="submit">Registrar</button>
        </p>

    </div>
</form>