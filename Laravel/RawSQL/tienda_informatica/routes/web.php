<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/add', 'ArticuloController@create');
Route::post('/add', 'ArticuloController@store');

Route::get('/list', 'ArticuloController@list');

Route::get('/delete/{id}', 'ArticuloController@delete');

Route::post('/show/', 'ArticuloController@show');

Route::get('/modify/{id}', 'ArticuloController@modifyShow');
Route::post('/modify', 'ArticuloController@modify');

Route::get('/deleteAll', 'ArticuloController@deleteAll');
