<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ArticuloController extends Controller {

    public function create() {
        $fabricantes = DB::table('fabricantes')->get();

        return view('añadir', ['fabricantes' => $fabricantes]);
    }

    public function list() {
        $fabricantes = DB::table('articulos')->get();
        return view('list', ['articulos' => $fabricantes]);
    }

    public function store(Request $request) {
        $this->validate($request, [
            'name' => 'required|max:255',
            'price' => 'required|numeric',
            'fabricante' => 'required|numeric',
        ]);

        DB::table('articulos')->insert(
                [
                    'nombre' => $request->name,
                    'precio' => $request->price,
                    'fabricante' => $request->fabricante
                ]
        );
        return redirect('list');
    }

    //Save modify
    public function modify(Request $request) {
        $this->validate($request, [
            'id' => 'exists:articulos',
            'name' => 'required|max:255',
            'price' => 'required|numeric',
            'fabricante' => 'required|numeric',
        ]);

        DB::table('articulos')
                ->where('id', $request->id)
                ->update([
                    'nombre' => $request->name,
                    'precio' => $request->price,
                    'fabricante' => $request->fabricante,
        ]);


        return redirect('list');
    }

    public function modifyShow($id) {
        $articulo = DB::table('articulos')->where('id', $id)->first();
        $fabricantes = DB::table('fabricantes')->get();

        if ($articulo && $fabricantes) {
            return view('modify', ['articulo' => $articulo, 'fabricantes' => $fabricantes]);
        } else {
            return "Articulo inexistente";
        }
    }
    
    //Show article
    public function show(Request $request) {
        $this->validate($request, [
            'id' => 'exists:articulos'
        ]);
        
        $articulo = DB::table('articulos')->where('id', $request->id)->first();
        $fabricantes = DB::table('fabricantes')->get();

        if ($articulo && $fabricantes) {
            return view('show', ['articulo' => $articulo, 'fabricantes' => $fabricantes]);
        } else {
            return "Articulo inexistente";
        }
    }
    
    //Delete article
    public function delete($id) {
        $articulo = DB::table('articulos')->where('id', '=', $id)->delete();

        if ($articulo) {
            return "Articulo borrado";
        } else {
            return "Articulo inexistente";
        }
    }

    public function deleteAll() {
        DB::table('articulos')->delete();
        return "Articulos borrados";
    }

}
