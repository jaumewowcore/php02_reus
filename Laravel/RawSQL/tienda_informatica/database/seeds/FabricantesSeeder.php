<?php

use Illuminate\Database\Seeder;
use App\Fabricante;

class FabricantesSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        for ($i = 1; $i <= 5; $i++) {
            
            Fabricante::create([
                'nombre' => "Fabricante $i"
            ]);
        }
    }

}
