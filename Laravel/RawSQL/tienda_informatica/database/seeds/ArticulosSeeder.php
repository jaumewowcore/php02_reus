<?php

use Illuminate\Database\Seeder;
use App\Articulo;

class ArticulosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
   public function run() {
        for ($i = 1; $i <= 5; $i++) {
            
            Articulo::create([
                'nombre' => "Articulo $i",
                'precio' => 10,
                'fabricante' => 1
            ]);
        }
        
        for ($i = 1; $i <= 5; $i++) {
            
            Articulo::create([
                'nombre' => "Articulo $i",
                'precio' => 8,
                'fabricante' => 2
            ]);
        }
    }
}
