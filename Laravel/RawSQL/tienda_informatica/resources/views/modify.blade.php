@extends('menu')

@section('sidebar')
@parent
@endsection

@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<h1>Modificar articulo</h1>
<br>
<form action="../modify" method="post">
    @csrf
    <p>
        <label>Nombre</label>
        <input type="hidden" name="id" required value="{{ $articulo->id }}" />
        <input type="text" name="name" required value="{{ $articulo->nombre }}" />
    </p>

    <p>
        <label>Precio</label>
        <input type="text" name="price" required value="{{ $articulo->precio }}" />
    </p>

    <p>
        <label>Fabricante</label>
        <select name="fabricante">
            @foreach($fabricantes as $fabricante)
            <option value="{{$fabricante->id}}"@if ($fabricante->id == $articulo->fabricante) selected @endif>{{$fabricante->nombre}}</option>
            @endforeach
        </select>
    </p>
    
    <input type="submit" value="Guardar">
</form>
@endsection