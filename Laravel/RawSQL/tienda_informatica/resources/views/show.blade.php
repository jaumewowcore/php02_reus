@extends('menu')

@section('sidebar')
@parent
@endsection

@section('content')


<h1>Mostrar articulo</h1>
<br>
    <p>
        <label>Nombre</label>
        <input type="hidden" name="id" required value="{{ $articulo->id }}" />
        <input type="text" name="name" required value="{{ $articulo->nombre }}" />
    </p>

    <p>
        <label>Precio</label>
        <input type="text" name="price" required value="{{ $articulo->precio }}" />
    </p>

    <p>
        <label>Fabricante</label>
        <select name="fabricante">
            @foreach($fabricantes as $fabricante)
            <option value="{{$fabricante->id}}"@if ($fabricante->id == $articulo->fabricante) selected @endif>{{$fabricante->nombre}}</option>
            @endforeach
        </select>
    </p>

@endsection