@extends('menu')

@section('sidebar')
@parent
@endsection

@section('content')



<h1>Listar articulos</h1>
<br>
<form method="post" action="{{url('show')}}">
    @csrf
    <p>
        <span>Escribe el id del articulo</span>
        <input type="number" name="id">
    </p>
    
    <input type="submit" value="Canviar" class="btn btn-info">                           
</form>
<br>
<table id="dtBasicExample" class="table table-striped table-bordered" cellspacing="0" width="100%" align="center">
    <thead>
        <tr align="center">
            <th class="th-sm">Id</th>
            <th class="th-sm">Nombre</th>
            <th class="th-sm">Precio</th>
            <th class="th-sm" colspan="2">OPERACIÓ</th>
        </tr>
    </thead>
    <tbody>
        @foreach($articulos as $articulo)
        <tr align="center">
            <td>{{$articulo->id}}</td>   
            <td>{{$articulo->nombre}}</td>   
            <td>{{$articulo->precio}}</td>   
            <td><a href="{{url("delete",$articulo->id)}}">Esborrar</a></td>
            <td><a href="{{url("modify",$articulo->id)}}">Modificar</a></td>
        </tr>
        @endforeach
    </tbody>
</table>            
@endsection