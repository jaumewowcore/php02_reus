<html>
    <head>
        <title>Tienda informatica</title>
    </head>
    <body>
        @section('sidebar')
        <ul id=”button”>
            <li><a href="add">Añadir registro</a></li>
            <li><a href="list">Listar</a></li>
            <li><a href="deleteAll">Borrar todo</a></li>
        </ul>
        @show

        <div class="container">
            @yield('content')
        </div>
    </body>
</html>

