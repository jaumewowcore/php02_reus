@extends('menu')

@section('sidebar')
@parent
@endsection

@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<h1>Añadir articulos</h1>
<br>
<form action="add" method="post">
    @csrf
    <p>
        <label>Nombre</label>
        <input type="text" name="name" required value="{{ old('name') }}" />
    </p>

    <p>
        <label>Precio</label>
        <input type="text" name="price" required value="{{ old('price') }}" />
    </p>

    <p>
        <label>Fabricante</label>
        <select name="fabricante">
            @foreach($fabricantes as $fabricante)
            <option value="{{$fabricante->id}}">{{$fabricante->nombre}}</option>
            @endforeach
        </select>
    </p>
    
    <input type="submit" value="Guardar">
</form>
@endsection