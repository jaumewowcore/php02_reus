<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap Core CSS -->
    <link href="{!! asset('themes/vendor/bootstrap/bootstrap.min.css') !!}" rel="stylesheet">

    <script src="{!! asset('themes/vendor/jquery-3.2.1.min.js') !!}"></script>   
    <script src="{!! asset('themes/vendor/bootstrap/bootstrap.min.js') !!}"></script>
    <script src="{!! asset('themes/vendor/bootstrap/popper.min.js') !!}"></script>
</head>

<div class="text-center mt-4">
    <img width="30%" src="{!! asset('themes/images/facebad.png') !!}" class="img-responsive center-block" />
    <h1 class="mt-2 mb-2">404</h1>
    <p class="h6">Page not found
    <p>The page you are looking for doesn't exist or another error ocurred.<br>
       Go back or head over to Scass Tech to choose a new direction</p>
</div>
    
