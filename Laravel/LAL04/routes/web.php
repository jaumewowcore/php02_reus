<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/test/lunes', function () {
    return '<br>Probando ruta con middleware';
})->middleware('lunes');

Route::match(['get', 'post', 'put', 'delete'], '/test/lunes', function () {
    return '<br>Probando rutas con metodos GET, POST, PUT, DELETE';
})->middleware('lunes');

Route::get('validar_dia', 'PrimerController@validar_dia');


Route::get('respuesta6', function () {
    return response("Esta página se refrescara en 5 segundos a Linkedin", 200)
           ->header('Cache-Control', 'max-age=3600')
           ->header('Refresh', '5; url=https://es.linkedin.com/');
});

Route::get('respuesta7', function () {
    return response()
            ->view('error')
            ->header('status', 404)
           ->header('Cache-Control', 'max-age=3600')
           ->header('Refresh', '5; url=/');
});