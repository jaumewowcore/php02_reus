<?php

namespace App\Http\Middleware;

use Closure;

class LunesMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $dias = array("domingo","lunes","martes","mi&eacute;rcoles","jueves","viernes","s&aacute;bado");
        
        echo date('d')."/".date('m')."/".date('Y')."<br>";

        if(date('w')=== '1'){
            echo "Bienvendo<br>";
        }

        echo "Hoy es ".$dias[date("w")];


        return $next($request);
    }
    
}
