<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PrimerController extends Controller
{
    public function __construct(){
        $this->middleware('lunes', ['only' => ['validar_dia']]);

    }

    public function validar_dia(){
        echo "<br>Método validar dia";
    }
}
