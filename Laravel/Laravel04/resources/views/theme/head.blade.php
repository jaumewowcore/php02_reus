<head>
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <!-- Bootstrap Core CSS -->
   <link href="{!! asset('themes/vendor/bootstrap/bootstrap.min.css') !!}" rel="stylesheet">
   <script src="{!! asset('themes/vendor/bootstrap/bootstrap.min.js') !!}"></script>
  
   <link href="{!! asset('themes/vendor/font-awesome-5/css/fontawesome-all.min.css') !!}" 
         rel="stylesheet" type="text/css">
</head>


