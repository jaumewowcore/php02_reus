<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>Personalizacion</title>
        <style>
            body{
                font-size: {{$fuente}};
            }
        </style>
    </head>    
    <body>
        <p>Fuente: {{$fuente}}</p>
        <form action="personalizacion" method="post">
        @csrf
        Fuente:
        <select name="fuente">
            <option value="24pt" @if($fuente == '24pt') selected @endif>Grande</option>
            <option value="16pt" @if($fuente == '16pt') selected @endif>Mediana</option>
            <option value="12pt" @if($fuente == '12pt') selected @endif>Pequeña</option>
        </select>
        <br>
        <input type="submit" value="Enviar">
            
        </form>
    </body>
    
</html>
