<!-- HEADER MOBILE-->
<header class="header-mobile d-block d-lg-none">
    <div class="header-mobile__bar">
        <div class="container-fluid">
            <div class="header-mobile-inner">
                <a class="logo" href="{!! asset('themes/index.html') !!}">
                    <img src="{!! asset('themes/images/icon/logo.png') !!}" alt="CoolAdmin" />
                </a>
                <button class="hamburger hamburger--slider" type="button">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    </div>
    <nav class="navbar-mobile">
        <div class="container-fluid">
            <ul class="navbar-mobile__list list-unstyled">
                <li class="has-sub">
                    <a class="js-arrow" href="#">
                        <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                    <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                        <li>
                            <a href="{!! asset('themes/index.html') !!}">Dashboard 1</a>
                        </li>
                        <li>
                            <a href="{!! asset('themes/index2.html') !!}">Dashboard 2</a>
                        </li>
                        <li>
                            <a href="{!! asset('themes/index3.html') !!}">Dashboard 3</a>
                        </li>
                        <li>
                            <a href="{!! asset('themes/index4.html') !!}">Dashboard 4</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="{!! asset('themes/chart.html') !!}">
                        <i class="fas fa-chart-bar"></i>Charts</a>
                </li>
                <li>
                    <a href="{!! asset('themes/table.html') !!}">
                        <i class="fas fa-table"></i>Tables</a>
                </li>
                <li>
                    <a href="{!! asset('themes/form.html') !!}">
                        <i class="far fa-check-square"></i>Forms</a>
                </li>
                <li>
                    <a href="#">
                        <i class="fas fa-calendar-alt"></i>Calendar</a>
                </li>
                <li>
                    <a href="map.html">
                        <i class="fas fa-map-marker-alt"></i>Maps</a>
                </li>
                <li class="has-sub">
                    <a class="js-arrow" href="#">
                        <i class="fas fa-copy"></i>Pages</a>
                    <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                        <li>
                            <a href="{!! asset('themes/login.html') !!}">Login</a>
                        </li>
                        <li>
                            <a href="{!! asset('themes/register.html') !!}">Register</a>
                        </li>
                        <li>
                            <a href="{!! asset('themes/forget-pass.html') !!}">Forget Password</a>
                        </li>
                    </ul>
                </li>
                <li class="has-sub">
                    <a class="js-arrow" href="#">
                        <i class="fas fa-desktop"></i>UI Elements</a>
                    <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                        <li>
                            <a href="{!! asset('themes/button.html') !!}">Button</a>
                        </li>
                        <li>
                            <a href="{!! asset('themes/badge.html') !!}">Badges</a>
                        </li>
                        <li>
                            <a href="{!! asset('themes/tab.html') !!}">Tabs</a>
                        </li>
                        <li>
                            <a href="{!! asset('themes/card.html') !!}">Cards</a>
                        </li>
                        <li>
                            <a href="{!! asset('themes/alert.html') !!}">Alerts</a>
                        </li>
                        <li>
                            <a href="{!! asset('themes/progress-bar.html') !!}">Progress Bars</a>
                        </li>
                        <li>
                            <a href="{!! asset('themes/modal.html') !!}">Modals</a>
                        </li>
                        <li>
                            <a href="{!! asset('themes/switch.html') !!}">Switchs</a>
                        </li>
                        <li>
                            <a href="{!! asset('themes/grid.html') !!}">Grids</a>
                        </li>
                        <li>
                            <a href="{!! asset('themes/fontawesome.html') !!}">Fontawesome Icon</a>
                        </li>
                        <li>
                            <a href="{!! asset('themes/typo.html') !!}">Typography</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
<!-- END HEADER MOBILE-->