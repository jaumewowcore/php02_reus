<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap Core CSS -->
    <link href="{!! asset('themes/vendor/bootstrap/bootstrap.min.css') !!}" rel="stylesheet">

    <script src="{!! asset('themes/vendor/jquery-3.2.1.min.js') !!}"></script>   
    <script src="{!! asset('themes/vendor/bootstrap/bootstrap.min.js') !!}"></script>
    <script src="{!! asset('themes/vendor/bootstrap/popper.min.js') !!}"></script>


    <!-- Fontfaces CSS-->
    <link href="{!! asset('themes/css/font-face.css') !!}" rel="stylesheet" type="text/css">
    <link href="{!! asset('themes/vendor/font-awesome-4.7/css/font-awesome.min.css') !!}" rel="stylesheet" type="text/css">
    <link href="{!! asset('themes/vendor/font-awesome-5/css/fontawesome-all.min.css') !!}" rel="stylesheet" type="text/css">
    <link href="{!! asset('themes/vendor/mdi-font/css/material-design-iconic-font.min.css') !!}" rel="stylesheet" type="text/css">
    
    <!-- Vendor CSS-->
    <link href="{!! asset('themes/vendor/animsition/animsition.min.css') !!}" rel="stylesheet" type="text/css">
    <link href="{!! asset('themes/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css') !!}" rel="stylesheet" type="text/css">
    <link href="{!! asset('themes/vendor/wow/animate.css') !!}" rel="stylesheet" type="text/css">
    <link href="{!! asset('themes/vendor/css-hamburgers/hamburgers.min.css') !!}" rel="stylesheet" type="text/css">
    <link href="{!! asset('themes/vendor/slick/slick.css') !!}" rel="stylesheet" type="text/css">
    <link href="{!! asset('themes/vendor/select2/select2.min.css') !!}" rel="stylesheet" type="text/css">
    <link href="{!! asset('themes/vendor/perfect-scrollbar/perfect-scrollbar.css') !!}" rel="stylesheet" type="text/css">

    <!-- Main CSS-->
    <link href="{!! asset('themes/css/theme.css') !!}" rel="stylesheet" type="text/css">
</head>

<a href="head.blade.php"></a>