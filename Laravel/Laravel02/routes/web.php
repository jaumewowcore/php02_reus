<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('paises', function () {
    return "Paises get";
});

Route::post('paises', function () {
    return "Paises post";
});

Route::post('paises/{pais}', function ($pais) {
    return "$pais post";
});

Route::put('paises/{pais}', function ($pais) {
    return "$pais put";
});

Route::delete('paises/{pais}', function ($pais) {
    return "$pais delete";
});

Route::post('paises/{pais}/departamentos', function ($pais) {
    return "$pais post departamentos";
});

Route::get('paises/{pais}/departamentos/{departamento?}', function ($pais, $departamento=1) {
    return "$pais get departamento $departamento";
});

Route::put('paises/{pais}/departamentos/{departamento?}', function ($pais, $departamento=1) {
    return "$pais put departamento $departamento";
});

Route::delete('paises/{pais}/departamentos/{departamento?}', function ($pais, $departamento=1) {
    return "$pais borrar departamento $departamento";
});


//Ejercicio 2
Route::any('example.com/colaboradores/{nombre}', function ($nombre) {
    return "Colaborador $nombre";
})->where(array("nombre" => "[a-zA-Z]+"));

Route::any('example.com/tienda/productos/{id}', function ($id) {
    return "Tienda $id";
})->where(array("id" => "[0-9]+"));

Route::any('example.com/agenda/julio/{ano}', function ($ano) {
    return "Tienda $ano";
})->where(array("ano" => "[0-9]+"));


Route::any('example.com/categoria/php', function () {
    return "Categorias ";
});

Route::any('example.com/categoria/php/{id}', function ($id) {
    return "Categorias  $id";
})->where(array("id" => "[0-9]+"));

